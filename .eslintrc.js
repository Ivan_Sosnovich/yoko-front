module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: __dirname,
        sourceType: 'module',
    },
    overrides: [
        {
            files: ["*.ts", "*.tsx"],
        }
    ],
    plugins: ['@typescript-eslint/eslint-plugin'],
    extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:prettier/recommended',
    ],
    root: true,
    env: {
        node: true,
        jest: true,
    },
    ignorePatterns: ['.eslintrc.js'],
    rules: {
        "no-unused-vars": "off",
        "react/require-default-props": 0,
        "prettier/prettier": "error",
        "@typescript-eslint/explicit-module-boundary-types": 0,
        "react/jsx-props-no-spreading": 0,
        "jsx-a11y/anchor-is-valid": 0,
        "react/react-in-jsx-scope": 0,
        "react/display-name": 0,
        "react/prop-types": 0,
        "@typescript-eslint/explicit-function-return-type": 0,
        "@typescript-eslint/explicit-member-accessibility": 0,
        "@typescript-eslint/indent": 0,
        "@typescript-eslint/member-delimiter-style": 0,
        "@typescript-eslint/no-explicit-any": 0,
        "@typescript-eslint/no-var-requires": 0,
        "no-use-before-define": 0,
        "@typescript-eslint/no-use-before-define": 0,
        "react/no-unescaped-entities": 0,
        "jsx-a11y/label-has-associated-control": 0,
        "react/no-unused-prop-types": 0,
        "no-underscore-dangle": 0,
        "@typescript-eslint/no-unused-vars": [
          0,
          {
            "argsIgnorePattern": "^_"
          }
        ],
        "no-console": [
          2,
          {
            "allow": ["warn", "error"]
          }
        ],
    },
};
