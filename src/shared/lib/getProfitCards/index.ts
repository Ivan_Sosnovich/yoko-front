import { coins } from "../../constants/coins";

type IProfitCards = {
  sum: string;
  currency: string;
  exchange: string;
  date: string;
};

export const getProfitCards = (): IProfitCards[] => {
  const MAX_CARDS_LENGTH = 50;
  let dateNow = new Date();
  const res: IProfitCards[] = [];

  while (res.length < MAX_CARDS_LENGTH) {
    const randomProfit = Math.random() * 100;
    const randomCoinIndex = Math.floor(Math.random() * coins.length);
    const exchange = coins[randomCoinIndex] + "/USDT";
    const randomDate = dateNow;
    randomDate.setMinutes(dateNow.getMinutes() - Math.random() * 6);
    randomDate.setSeconds(dateNow.getSeconds() - Math.random() * 6);
    res.push({
      sum: `${randomProfit.toFixed(3)}`,
      currency: "USDT",
      exchange,
      date: randomDate.toLocaleString("ru-GB"),
    });

    dateNow = randomDate;
  }

  return res;
};
