import { getFormatDate } from "../getFormatDate";

export const getFormatPeriod = (period: string) => {
  const [start, end] = period.split("-");
  return getFormatDate(start) + "-" + getFormatDate(end);
};
