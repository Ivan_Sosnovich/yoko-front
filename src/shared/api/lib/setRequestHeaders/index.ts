import { LOCAL_STORAGE_TOKEN_NAME } from "shared/constants/variable";

export const setRequestHeaders = () => {
  const token = window.localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME);
  const headers: Record<string, string> = {};
  if (token) {
    headers["Authorization"] = "Bearer " + token;
  }
  return headers;
};
