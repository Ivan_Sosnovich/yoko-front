import http from "shared/api/axios";

export const sessionApi = () => http.get("user/session");
