export enum UserRole {
  ADMIN = "admin",
  SUPER_ADMIN = "superAdmin",
  USER = "user",
}

export type IUser = {
  id?: number;
  name: string;
  email: string;
  password: string;
  balance: number;
  tradingBalance: number;
  referralCode: string;
  referralId?: number;
  isActive: boolean;
  referralBalance: number;
  role: UserRole;
  bonusBalance: number;
  code?: string;
};
