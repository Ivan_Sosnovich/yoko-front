import styled from "@emotion/styled";

export const SuccessIconBlockWrapper = styled.div`
  width: 149px;
  height: 149px;

  @media screen and (max-width: 670px) {
    width: 112px;
    height: 112px;
  }
`;

export const SuccessIcon = styled.img`
  object-fit: contain;
  max-width: 149px;
  max-height: 149px;

  @media screen and (max-width: 670px) {
    max-width: 112px;
    max-height: 112px;
  }
`;
