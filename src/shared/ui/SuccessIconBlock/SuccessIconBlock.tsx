import SuccessIconPath from "shared/image/successIcon.png";
import * as Styled from "./SuccessIconBlock.styled";

const SuccessIconBlock = () => {
  return (
    <Styled.SuccessIconBlockWrapper>
      <Styled.SuccessIcon src={SuccessIconPath} alt="icon for succses" />
    </Styled.SuccessIconBlockWrapper>
  );
};

export default SuccessIconBlock;
