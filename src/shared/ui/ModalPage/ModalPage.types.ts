import React from "react";

export type IModalPageProps = {
  children: React.ReactElement;
  onClose: () => void;
};
