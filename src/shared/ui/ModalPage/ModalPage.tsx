import { useEffect } from "react";
import CloseIcon from "shared/icons/close.svg";
import { IModalPageProps } from "./ModalPage.types";
import * as Styled from "./ModalPage.styled";

const ModalPage = (props: IModalPageProps) => {
  const { children, onClose } = props;

  const handleClickWidow = (ev) => {
    const clickedElement = ev.target as HTMLDivElement;
    const modalPage = document.getElementById("modal-page")!;
    const its_menu = clickedElement == modalPage || modalPage.contains(clickedElement);
    if (!its_menu) {
      onClose();
    }
  };

  useEffect(() => {
    document.body.classList.add("body-overflow");
    return () => document.body.classList.remove("body-overflow");
  }, []);

  useEffect(() => {
    document.addEventListener("mousedown", handleClickWidow);
    return () => document.removeEventListener("mousedown", handleClickWidow);
  }, []);

  return (
    <Styled.ModalPageWrapper id="modal-page">
      <Styled.ModalPageContent>
        <Styled.ModalPageClosedButton onClick={() => onClose()}>
          <CloseIcon />
        </Styled.ModalPageClosedButton>
        {children}
      </Styled.ModalPageContent>
    </Styled.ModalPageWrapper>
  );
};

export default ModalPage;
