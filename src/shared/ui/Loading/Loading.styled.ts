import styled from "@emotion/styled";

export const LoadingWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: none;
  z-index: 1000;
  display: flex;
  align-items: center;
  justify-content: center;
  justify-items: center;
  align-content: center;
`;
