import CircularProgress from "@mui/material/CircularProgress";
import * as Styled from "./Loading.styled";
const Loading = () => {
  return (
    <Styled.LoadingWrapper>
      <CircularProgress color="secondary" />
    </Styled.LoadingWrapper>
  );
};

export default Loading;
