export type ITableProps = {
  title?: string;
  prefix?: string;
  emptyMessage?: string;
  pageCount?: number;
  activePage?: number;
  headCells?: IHeadCell[];
  rows?: IRow[];
  handleChangeActivePage?: (num: number) => void;
  isLoading?: boolean;
  isScrollHidden?: boolean;
  rowFlexPosition?: string;
};

export type IHeadCell = {
  width: string;
  content: string;
  position: string;
  flexPosition: string;
};

export type IRow = {
  handleClick?: () => void;
  cells: IRowCell[];
  mobileCard: ITableCard;
};

export type IRowCell = {
  width: string;
  content: string;
  position: string;
  flexPosition: string;
  textLink?: string;
  statusType?: string;
  isNotTranslate?: boolean;
  variable?: string;
  type: "text" | "link" | "status";
  color?: string;
};

export type ITableCard = {
  title?: string;
  description?: string;
  descriptionBlock?: IDescriptionBlock;
  tableBlock?: ITableBlock[];
};

type IDescriptionBlock = {
  type: "table" | "status";
  status?: string;
  title?: string;
  description?: string;
  tableCell?: ITableBlock;
};

export type ITableBlock = {
  type: "text" | "link";
  title: string;
  value: string;
  position: string;
  textLink?: string;
  isNotTranslate?: boolean;
  variable?: string;
  color?: string;
  maxWidth?: string;
};
