import { useTranslation } from "react-i18next";
import { isMobile } from "react-device-detect";
import Pagination from "@mui/material/Pagination";
import Loading from "shared/ui/Loading";
import CellWrapper from "./components/CellWrapper/CellWrapper";
import TableCard from "./components/TableCard/TableCard";
import { ITableProps } from "./Table.types";
import * as Styled from "./Table.styled";

const Table = (props: ITableProps) => {
  const {
    title,
    prefix,
    emptyMessage,
    pageCount,
    activePage,
    headCells,
    rows,
    isLoading,
    handleChangeActivePage,
    isScrollHidden,
    rowFlexPosition,
  } = props;
  const { t } = useTranslation();
  if (isLoading && !headCells?.length) {
    return (
      <Styled.HomePageEmptyMessageBlock>
        <Loading />
      </Styled.HomePageEmptyMessageBlock>
    );
  }

  if (emptyMessage) {
    return (
      <Styled.HomePageEmptyMessageBlock>
        <Styled.HomePageEmptyMessage>{t(emptyMessage)}</Styled.HomePageEmptyMessage>
      </Styled.HomePageEmptyMessageBlock>
    );
  }
  return (
    <Styled.TableContentWrapper {...{ isMobile }}>
      {title && (
        <Styled.TableContentTitle {...{ isMobile }}>
          {t(title)}
          {prefix && <span>{prefix}</span>}
        </Styled.TableContentTitle>
      )}
      {headCells?.length && rows?.length && (
        <>
          <Styled.TableWrapper {...{ isMobile, isScrollHidden }}>
            {!isMobile && (
              <>
                <Styled.TableHeader {...{ rowFlexPosition }}>
                  {headCells.map(({ width, content, position, flexPosition }) => (
                    <Styled.TableHeadCell {...{ width, position, flexPosition }}>
                      <p>{t(content)}</p>
                    </Styled.TableHeadCell>
                  ))}
                </Styled.TableHeader>
                {isLoading && <Loading />}
                {!isLoading && (
                  <Styled.TableBody>
                    {rows.map(({ cells, handleClick }) => (
                      <Styled.TableRow
                        onClick={() => {
                          if (handleClick) {
                            handleClick();
                          }
                        }}
                        isHover={!!handleClick}
                        {...{ rowFlexPosition }}
                      >
                        {cells.map((cell) => (
                          <Styled.TableRowCell
                            {...{
                              width: cell.width,
                              position: cell.position,
                              flexPosition: cell.flexPosition,
                            }}
                          >
                            <CellWrapper {...cell} />
                          </Styled.TableRowCell>
                        ))}
                      </Styled.TableRow>
                    ))}
                  </Styled.TableBody>
                )}
              </>
            )}
            {isMobile && (
              <>
                {isLoading && <Loading />}
                {!isLoading && (
                  <>
                    {rows.map(({ mobileCard, handleClick }) => (
                      <TableCard
                        handleSelectCard={() => {
                          if (handleClick) {
                            handleClick();
                          }
                        }}
                        isHover={!!handleClick}
                        {...{ mobileCard }}
                      />
                    ))}
                  </>
                )}
              </>
            )}
          </Styled.TableWrapper>
          {!!pageCount && (
            <Styled.TablePaganationBlock {...{ isMobile }}>
              <Pagination
                color="primary"
                count={pageCount}
                defaultPage={activePage + 1}
                onChange={(_, page) => handleChangeActivePage(page)}
              />
            </Styled.TablePaganationBlock>
          )}
        </>
      )}
    </Styled.TableContentWrapper>
  );

  return null;
};

export default Table;
