import { useTranslation } from "react-i18next";
import { ITableCardProps } from "./TableCard.types";
import * as Styled from "./TableCard.styled";
import StatusBadge from "../StatusBadge/StatusBadge";
import { IStatusBadgeProps } from "../StatusBadge/StatusBadge.types";
import TableBlock from "../TableBlock/TableBlock";

const TableCard = (props: ITableCardProps) => {
  const { t } = useTranslation();

  const {
    handleSelectCard,
    isHover,
    mobileCard: { title, description, descriptionBlock, tableBlock },
  } = props;

  return (
    <Styled.TableCardWrapper onClick={() => handleSelectCard()} {...{ isHover }}>
      {(title || description || descriptionBlock) && (
        <>
          <Styled.TableCardMainWrapper isNotBorder={!tableBlock}>
            {(title || description) && (
              <Styled.TableCardMainInfomation>
                {title && (
                  <Styled.TableCardMainInfomationTitle>
                    {t(title)}
                  </Styled.TableCardMainInfomationTitle>
                )}
                {description && (
                  <Styled.TableCardMainInfomationDescription>
                    {description}
                  </Styled.TableCardMainInfomationDescription>
                )}
              </Styled.TableCardMainInfomation>
            )}
            {descriptionBlock && (
              <Styled.DescriptionBlockWrapper>
                {descriptionBlock?.type === "status" && (
                  <StatusBadge
                    status={descriptionBlock.status! as IStatusBadgeProps["status"]}
                    content={descriptionBlock.title!}
                  />
                )}
                {descriptionBlock?.type === "table" && (
                  <Styled.TableCell>
                    <Styled.TableCellTitle>
                      {t(descriptionBlock.tableCell.title)}
                    </Styled.TableCellTitle>
                    <Styled.TableCellValue>
                      {descriptionBlock.tableCell.variable &&
                        !descriptionBlock.tableCell.isNotTranslate &&
                        t(descriptionBlock.tableCell.value, {
                          variable: descriptionBlock.tableCell.variable,
                        })}
                      {!descriptionBlock.tableCell.variable &&
                        (!descriptionBlock.tableCell.isNotTranslate
                          ? t(descriptionBlock.tableCell.value)
                          : descriptionBlock.tableCell.value)}
                    </Styled.TableCellValue>
                  </Styled.TableCell>
                )}
              </Styled.DescriptionBlockWrapper>
            )}
          </Styled.TableCardMainWrapper>
        </>
      )}
      {tableBlock && <TableBlock {...{ tableBlock }} />}
    </Styled.TableCardWrapper>
  );
};

export default TableCard;
