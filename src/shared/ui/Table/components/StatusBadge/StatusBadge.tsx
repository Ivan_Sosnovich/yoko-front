import { useTranslation } from "react-i18next";
import type { IStatusBadgeProps } from "./StatusBadge.types";
import * as Styled from "./StatusBadge.styled";

const StatusBadge = (props: IStatusBadgeProps) => {
  const { t } = useTranslation();
  const { content, status } = props;
  switch (status) {
    case "expired":
      return <Styled.SuccessStatus>{t(content)}</Styled.SuccessStatus>;
    case "error":
      return <Styled.ErrorStatus>{t(content)}</Styled.ErrorStatus>;
    case "pending":
      return <Styled.PendingStatus>{t(content)}</Styled.PendingStatus>;
    case "processed":
      return <Styled.ProcessedStatus>{t(content)}</Styled.ProcessedStatus>;
    case "init":
      return <Styled.ExpiredStatus>{t(content)}</Styled.ExpiredStatus>;
    default:
      return <Styled.ProcessedStatus>{t(content)}</Styled.ProcessedStatus>;
  }
};

export default StatusBadge;
