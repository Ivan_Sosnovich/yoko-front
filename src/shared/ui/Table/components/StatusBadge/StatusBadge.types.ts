import { IDepositStatus } from "widgets/paymentTable/model/api/getDepositApi";

export type IStatusBadgeProps = {
  content: string;
  status?: IDepositStatus;
};
