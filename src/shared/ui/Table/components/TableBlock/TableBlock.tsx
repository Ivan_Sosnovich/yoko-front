import { useTranslation } from "react-i18next";
import { ITableCard } from "../../Table.types";
import * as Styled from "./TableBlock.styled";
import CellWrapper from "../CellWrapper/CellWrapper";
import LinkBadge from "../LinkBadge/LinkBadge";
import TextBadge from "../TextBadge/TextBadge";

const TableBlock = (props: Pick<ITableCard, "tableBlock">) => {
  const { tableBlock } = props;
  const { t } = useTranslation();

  return (
    <Styled.TableBlockWrapper>
      <Styled.TableBlockBody>
        {tableBlock.map(
          ({
            type,
            value,
            title,
            position,
            textLink,
            isNotTranslate,
            color,
            variable,
            maxWidth,
          }) => (
            <Styled.TableBlockRow>
              <Styled.TableTitleCell>{t(title)}</Styled.TableTitleCell>
              <Styled.TableValueCell {...{ maxWidth }}>
                {type === "link" && <LinkBadge content={value} {...{ textLink, position }} />}
                {type === "text" && (
                  <TextBadge content={value} {...{ position, isNotTranslate, color, variable }} />
                )}
              </Styled.TableValueCell>
            </Styled.TableBlockRow>
          )
        )}
      </Styled.TableBlockBody>
    </Styled.TableBlockWrapper>
  );
};

export default TableBlock;
