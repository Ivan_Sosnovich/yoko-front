import styled from "@emotion/styled";

export const TableBlockWrapper = styled.table`
  width: 100%;
`;

export const TableBlockBody = styled.tbody`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 16px;
`;

export const TableBlockRow = styled.tr`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 16px;
  width: 100%;
`;

export const TableTitleCell = styled.td`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.2px;
  color: #9298b8;
  width: fit-content;
  min-width: 110px;
`;

type IMaxWidth = {
  maxWidth?: string;
};

export const TableValueCell = styled.td<IMaxWidth>`
  max-width: 150px;
  width: fit-content;
  & > p {
    width: fit-content;
    max-width: ${({ maxWidth }) => (maxWidth ? maxWidth : "150px")};
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    word-wrap: break-word;
  }
`;
