import styled from "@emotion/styled";
import { IRowCell } from "../../Table.types";

export const LinkBadgeWrapper = styled.a<Pick<IRowCell, "content" | "textLink" | "position">>`
  text-decoration: none;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  display: flex;
  text-align: center;
  letter-spacing: 0.2px;
  color: #5f5cec;
  cursor: pointer;
  text-align: ${({ position }) => position};
`;
