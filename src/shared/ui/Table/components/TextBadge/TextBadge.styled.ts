import styled from "@emotion/styled";
import { IRowCell } from "../../Table.types";

export const TextBadgeWrapper = styled.p<Pick<IRowCell, "content" | "position">>`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  display: flex;
  align-items: center;
  letter-spacing: 0.2px;
  color: ${({ color }) => (color ? color : "#181938")};
  text-align: ${({ position }) => position};
`;
