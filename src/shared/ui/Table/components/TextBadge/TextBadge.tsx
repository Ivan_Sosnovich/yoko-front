import { useTranslation } from "react-i18next";
import { IRowCell } from "../../Table.types";
import * as Styled from "./TextBadge.styled";

const TextBadge = (
  props: Pick<IRowCell, "content" | "position" | "isNotTranslate" | "color" | "variable">
) => {
  const { t } = useTranslation();
  const { content, isNotTranslate, variable } = props;
  return (
    <Styled.TextBadgeWrapper {...props}>
      {variable && !isNotTranslate && t(content, { variable })}
      {!variable && (!isNotTranslate ? t(content) : content)}
    </Styled.TextBadgeWrapper>
  );
};

export default TextBadge;
