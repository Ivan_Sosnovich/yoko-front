import Button, { ButtonProps } from "@mui/material/Button";
import { styled } from "@mui/material/styles";

interface IButtonStyledProps extends ButtonProps {
  width: string;
}

export const ButtonWrapper = styled(Button)(({ theme, size, color }) => ({
  position: "relative",
  width: size === "small" ? "134px" : "217px",
  height: size === "small" ? "44px" : "54px",
  background: color === "primary" ? theme.palette.colorAccent : "#F4F7FE",
  color: color === "primary" ? theme.palette.colorWhite : "#5F5CEC",
  boxSizing: "border-box",
  padding: "0 5px",
  borderRadius: size === "small" ? "10px" : "15px",
  boxShadow: "none",
  ":hover": {
    background: color === "primary" ? "#8785F1" : "#F4F7FE",
    boxShadow: color === "secondary" ? "0 5px 10px 2px rgba(34, 60, 80, 0.2) inset" : "none",
  },
  ":disabled": {
    background: "#E8EAF0",
  },
  ":active": {
    background: "#4643D2",
  },
  ...(size === "small" ? theme.typography.buttonMobile : theme.typography.button),
}));
