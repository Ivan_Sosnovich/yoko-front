import styled from "@emotion/styled";

export const ActionsCardWrapper = styled.div`
  position: relative;
  padding: 8px 14px 8px 14px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 35%;
  height: 76px;
  background: #ffffff;
  box-shadow: 0 25px 20px -25px rgba(20, 25, 143, 0.15);
  border-radius: 20px;
  box-sizing: border-box;
  gap: 5px;

  @media screen and (max-width: 1392px) {
    width: 100%;
  }
`;

export const ActionsCardMainBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex: 8 0 70%;
  gap: 10px;
  max-width: 70%;
`;
export const ActionsCardInformationBlock = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-flow: column nowrap;
  box-sizing: border-box;
  gap: 8px;
`;

export const ActionsCardTitle = styled.h2`
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.3px;
  color: #181938;
`;

export const ActionsCardDescriptionBlock = styled.div`
  display: flex;
  align-items: center;
  justify-items: center;
  flex-wrap: wrap;
  box-sizing: border-box;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: pre-line;
  width: "100%;
`;

type ICardDescription = {
  color: string;
};
export const ActionsCardDescription = styled.p<ICardDescription>`
  font-style: normal;
  font-weight: 900;
  font-size: 24px;
  line-height: 24px;
  letter-spacing: 0.3px;
  color: ${({ color }) => color};
  text-overflow: ellipsis;
  overflow: hidden;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  display: -webkit-box;
  white-space: normal;
`;

export const ActionsCardActionsBLock = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex: 2 0 30%;
  max-width: 30%;
`;

export const ActionsCardPopupMenuWrapper = styled.div`
  cursor: pointer;
`;
