export type IActionsCard = {
  Icon: React.FC;
  title: string;
  descriptionColor?: string;
  description?: string;
  errorInfo?: string | null;
  info?: string;
  actions?: React.FC[];
  Action?: React.FC;
  isLoading?: boolean;
};
