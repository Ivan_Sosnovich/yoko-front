import React from "react";
import * as Styled from "./InputField.styled";

interface InputFieldProps {
  placeholder?: string;
  handleChange: (evt: React.ChangeEvent<any>) => void;
  name: string;
  error?: string;
  type: string;
  value?: string;
}

const InputField = ({ placeholder, handleChange, name, error, type, value }: InputFieldProps) => {
  return (
    <>
      <Styled.InputField
        onChange={(evt) => handleChange(evt)}
        {...{ placeholder, name, type, value }}
      />
      {error && <Styled.InputErrorBlock>{error}</Styled.InputErrorBlock>}
    </>
  );
};

export default InputField;
