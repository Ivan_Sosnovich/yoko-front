import { Swiper, SwiperSlide } from "swiper/react";
import { SliderProps } from "./Slider.types";

//TODO типизировать пропсы

const Slider = (props: SliderProps) => {
  const { items, settings } = props;
  return (
    <Swiper {...settings}>
      {items.map((item) => (
        <SwiperSlide>{item}</SwiperSlide>
      ))}
    </Swiper>
  );
};

export default Slider;
