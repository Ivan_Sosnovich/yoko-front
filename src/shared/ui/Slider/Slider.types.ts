import { ReactElement } from "react";
import { Navigation, Pagination } from "swiper";

export interface SliderProps {
  items: Array<ReactElement>;
  settings: SliderSettings;
}

export interface SliderSettings {
  modules: ModulesType[];
  navigation: NavigationElementType;
  pagination: PaginationElementType;
  grabCursor: boolean;
  slidesPerView: number;
  spaceBetween: number;
  loop: boolean;
  breakpoints: Record<number, BreakPointsItem>;
}

type PaginationElementType = {
  el: string;
  clickable: boolean;
};
type NavigationElementType = {
  nextEl: string;
  prevEl: string;
};
type NavigationType = typeof Navigation;
type PaginationType = typeof Pagination;
type ModulesType = NavigationType | PaginationType;
type BreakPointsItem = {
  slidesPerView: number;
  spaceBetween: number;
};
