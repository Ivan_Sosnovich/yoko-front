export enum PATH {
  MAIN = "/",
  HOME = "/home",
  POSITION = "/position",
  REFERRAL = "/referral",
  PAYMENT = "/payment",
}
