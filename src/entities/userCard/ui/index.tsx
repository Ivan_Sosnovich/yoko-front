import { useAppSelector } from "providers";
import UserLogout from "features/userLogout";
import DefaultUserAvatar from "shared/image/defaultAvatar.png";
import { getUserSelector } from "../model/selectors/getUserSelector";
import { IUserCardProps } from "./index.types";
import * as Styled from "./index.styled";

const UserCard = (props: IUserCardProps) => {
  const { isSideBar } = props;
  const { name, id, email } = useAppSelector(getUserSelector)!;

  return (
    <Styled.UserCardWrapper {...{ isSideBar }}>
      <Styled.UserCardAvatarBlock>
        <img src={DefaultUserAvatar} alt="user avatar" />
      </Styled.UserCardAvatarBlock>
      <Styled.UserCardInformationWrapper>
        <Styled.UserCardInformationBlock>
          <Styled.UserName>{name}</Styled.UserName>
          <Styled.UserID>{`ID: ${id}`}</Styled.UserID>
        </Styled.UserCardInformationBlock>
        <Styled.UserEmail>{email}</Styled.UserEmail>
      </Styled.UserCardInformationWrapper>
      <UserLogout />
    </Styled.UserCardWrapper>
  );
};

export default UserCard;
