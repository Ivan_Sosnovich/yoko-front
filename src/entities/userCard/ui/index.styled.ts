import styled from "@emotion/styled";
import { IUserCardProps } from "./index.types";

export const UserCardWrapper = styled.div<IUserCardProps>`
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
  max-width: 292px;
  height: 52px;
  background: ${({ isSideBar }) => (isSideBar ? "rgba(244, 247, 254, 0.1)" : "#f4f7fe")};
  border-radius: 5px;
  box-sizing: border-box;
  gap: 10px;
`;

export const UserCardAvatarBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  border-radius: 12.5px;
  max-width: 40px;
  max-height: 40px;
  background: #a3aed0;
  box-sizing: border-box;

  & > img {
    width: 24px;
    height: 24px;
    object-fit: contain;
    max-width: 24px;
    max-height: 24px;
  }
`;

export const UserCardInformationWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  gap: 2px;
`;

export const UserCardInformationBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  gap: 5px;
  box-sizing: content-box;
`;

export const UserName = styled.h3`
  font-weight: 700;
  font-size: 13px;
  line-height: 13px;
  letter-spacing: 0.3px;
  color: #181938;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const UserID = styled.h3`
  font-weight: 400;
  font-size: 11px;
  line-height: 11px;
  letter-spacing: 0.3px;
  color: #9298b8;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const UserEmail = styled.h3`
  font-weight: 400;
  font-size: 13px;
  line-height: 13px;
  letter-spacing: 0.3px;
  color: #181938;
  max-width: 114px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;
