import styled from "@emotion/styled";

export const SocialBlock = styled.div`
  display: flex;
  align-items: center;
  justify-items: flex-start;
  gap: 24px;
`;
