import UserMainBalanceCard from "widgets/userMainBalanceCard";
import BotInformationCard from "widgets/botInformationCard";
import TraidingBalanceCard from "widgets/tradingBalanceCard";
import ReferralCountCard from "widgets/referralCountCard";
import ReferralCodeCard from "widgets/referralCodeCard";
import ReferralBalanceCard from "widgets/referralBalanceCard";
import { CardsType } from "../config/index.constants";
import type { ICardsComponentsProps } from "../index.types";

const CardsComponent = (props: ICardsComponentsProps) => {
  const { type } = props;

  switch (type) {
    case CardsType.USER_MAIN_BALANCE:
      return <UserMainBalanceCard />;
    case CardsType.TRADING_BALANCE:
      return <TraidingBalanceCard />;
    case CardsType.BOT_ACTIONS:
      return <BotInformationCard />;
    case CardsType.REFERRAL_BALANCE:
      return <ReferralBalanceCard />;
    case CardsType.REFERRAL_CODE:
      return <ReferralCodeCard />;
    case CardsType.REFERRAL_COUNT:
      return <ReferralCountCard />;
    default:
      return null;
  }
};

export default CardsComponent;
