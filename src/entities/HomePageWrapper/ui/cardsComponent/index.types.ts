import { CardsType } from "./config/index.constants";

export type ICardsComponentsProps = {
  type: CardsType;
};
