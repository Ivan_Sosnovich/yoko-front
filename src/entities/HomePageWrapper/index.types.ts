import { EChartsOption } from "echarts-for-react";
import type { ITableType, CardsType } from "./";

export type IHomePageWrapperProps = {
  title: string;
  eChartsOption?: EChartsOption;
  tableType?: ITableType;
  cards?: CardsType[];
};
