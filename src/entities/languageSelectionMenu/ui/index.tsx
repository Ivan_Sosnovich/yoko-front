import { useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import RUIcon from "shared/icons/ru.svg";
import ENIcon from "shared/icons/usa.svg";
import { getSelectedLanguage } from "../lib";
import * as Styled from "./index.styled";

const LanguageSelectionMenu = () => {
  const { i18n } = useTranslation();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const selectedLanguage = getSelectedLanguage();

  const handleChangeLanguage = (language: string) => {
    i18n.changeLanguage(language);
    setAnchorEl(null);
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        aria-controls={!!anchorEl ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={!!anchorEl ? "true" : undefined}
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
      >
        <Styled.LngInformationBlock>
          <Styled.LngText>{selectedLanguage !== "ru" ? "En" : "Ru"}</Styled.LngText>
          {selectedLanguage === "ru" ? <RUIcon /> : <ENIcon />}
        </Styled.LngInformationBlock>
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={!!anchorEl}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
        sx={{
          borderRadius: "15px",
        }}
      >
        <MenuItem
          onClick={() => handleChangeLanguage("ru")}
          sx={{ justifyContent: "space-between" }}
        >
          <Styled.LngInformationBlock>
            <Styled.LngText>Ru</Styled.LngText>
            <RUIcon />
          </Styled.LngInformationBlock>
        </MenuItem>
        <MenuItem
          onClick={() => handleChangeLanguage("en")}
          sx={{ justifyContent: "space-between" }}
        >
          <Styled.LngInformationBlock>
            <Styled.LngText>En</Styled.LngText>
            <ENIcon />
          </Styled.LngInformationBlock>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default LanguageSelectionMenu;
