export type ITransferModalProps = {
  isOpen: boolean;
  mainError?: string;
  type: "add" | "remove";
  handleCLose: () => void;
  from: IInputProps;
  to: IInputProps;
  title: string;
  action: IActionProps;
  isLoading: boolean;
};

type IInputProps = {
  label?: string;
  placeholder?: string;
  max?: number;
  min?: number;
  initValue?: number;
};

type IActionProps = {
  title: string;
  action: (deposit: number) => void;
};

export type ITransferModalFormValue = {
  fromValue: string;
  toValue: string;
};
