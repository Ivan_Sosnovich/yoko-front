import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "providers";
import type { IBotSettingsData } from "../../../";
import { changeBotSettingsApi } from "../../";

export const changeBotSettingsAction = createAsyncThunk<
  IBotSettingsData,
  IBotSettingsData,
  AsyncThunkConfig
>("bot/change/settings", async (changeBotSettingsDto, { rejectWithValue }) => {
  try {
    return await changeBotSettingsApi(changeBotSettingsDto);
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
