import http from "shared/api/axios";
import { IBotSettingsData } from "../../../";

export const changeBotSettingsApi = (changeBotSettingsDto: IBotSettingsData) =>
  http.post<IBotSettingsData, IBotSettingsData>("bot/settings", changeBotSettingsDto);
