import { IStore } from "providers";

export const getChangeSettingsSelector = (state: IStore) => state.botReducer.settings;
