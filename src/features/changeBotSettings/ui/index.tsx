import { useTranslation } from "react-i18next";
import { isMobile } from "react-device-detect";
import { Formik } from "formik";
import { useAppSelector, useAppDispatch } from "providers";
import Button from "shared/ui/Button";
import {
  getChangeSettingsSelector,
  changeBotSettingsAction,
  getIsLoadingChangeBotSettingsSelectors,
} from "../model";
import BooleanSelect from "./BooleanSelect";
import InputNumber from "./InputNumber";
import InputSelect from "./InputSelect";
import type { IBotSettingsData } from "./index.types";
import * as Styled from "./index.styled";

const ChangeBotSettings = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const changeBotSettingsData = useAppSelector(getChangeSettingsSelector);
  const isLoading = useAppSelector(getIsLoadingChangeBotSettingsSelectors);

  const handleSubmit = (values: IBotSettingsData) => {
    dispatch(changeBotSettingsAction(values));
  };

  return (
    <Formik onSubmit={handleSubmit} initialValues={changeBotSettingsData}>
      {({ handleSubmit }) => (
        <Styled.ChangeBotSettingsWrapper>
          <Styled.ChangeBotSettingsBlock>
            <Styled.TextFieldTitle>
              {t("table.bot.change.bot.settings.main.title")}
            </Styled.TextFieldTitle>
            <Styled.TextFieldWrapper>
              <Styled.TextFieldBlock>
                <InputSelect name="allowedPairs" label="Allowed coins" />
                <InputSelect name="blockedPairs" label="Blocked pairs" />
                <InputNumber name="minBnb" label="Min bnb" />
                <InputNumber name="minBalance" label="Min balance" />
                <InputNumber name="minOrder" label="Min order" />
                <InputNumber name="minDailyPercent" label="Min daily percent" />
              </Styled.TextFieldBlock>
              <Styled.TextFieldBlock>
                <InputNumber name="dailyPercent" label="Daily percent" />
                <InputNumber name="orderTimer" label="Order timer" />
                <InputNumber name="minValue" label="Min value" />
                <InputNumber name="buyDown" label="Buy down" />
                <InputNumber name="sellUp" label="Sell Up" />
                <InputNumber name="maxTraidePairs" label="Max trade pairs" />
                <InputNumber name="maxAver" label="Max Aver" />
              </Styled.TextFieldBlock>
            </Styled.TextFieldWrapper>
          </Styled.ChangeBotSettingsBlock>
          <Styled.ChangeBotSettingsBlock>
            <Styled.TextFieldTitle>
              {t("table.bot.change.bot.settings.second.title")}
            </Styled.TextFieldTitle>
            <Styled.TextFieldWrapper>
              <Styled.TextFieldBlock>
                <BooleanSelect name="progressiveMaxPairs" label="Progressive max pairs" />
                <BooleanSelect name="numAver" label="Num aver" />
                <InputNumber name="stepAver" label="Step aver" />
                <InputNumber name="quantityAver" label="Quantity aver" />
              </Styled.TextFieldBlock>
              <Styled.TextFieldBlock>
                <BooleanSelect name="progressiveAverage" label="Progressive average" />
                <InputNumber name="averagePercent" label="Average percent" />
                <BooleanSelect name="trailingStop" label="Trailing stop" />
                <InputNumber name="trailingPercent" label="Trailing percent" />
              </Styled.TextFieldBlock>
            </Styled.TextFieldWrapper>
          </Styled.ChangeBotSettingsBlock>
          <Button
            color="primary"
            size={isMobile ? "small" : "medium"}
            {...{ isLoading }}
            onClick={() => handleSubmit()}
          >
            {t("table.bot.change.keys.button")}
          </Button>
        </Styled.ChangeBotSettingsWrapper>
      )}
    </Formik>
  );
};

export default ChangeBotSettings;
