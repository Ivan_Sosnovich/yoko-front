import { useFormikContext } from "formik";
import get from "lodash/get";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import * as Styled from "./index.styled";

type BooleanFieldProps = {
  name: string;
  label: string;
};

const BooleanSelect = (props: BooleanFieldProps) => {
  const { name, label } = props;
  const { values, setFieldValue } = useFormikContext();
  const value = get(values, name, "");
  const handleChangeFields = (value: string) => {
    const newValue = value === "Yes";
    setFieldValue(name, newValue);
  };
  const onChange = () => {
    return;
  };
  return (
    <Styled.BooleanSelectWrapper>
      <Styled.BooleanSelectLabel htmlFor="uncontrolled-native">{label}</Styled.BooleanSelectLabel>
      <Styled.BooleanInputBlock>
        <Styled.BooleanInput {...{ name, onChange }} value={value ? "Yes" : "No"} />
        <Styled.BooleanActionsBlock>
          <KeyboardArrowUpIcon
            color="info"
            fontSize="small"
            onClick={() => handleChangeFields("Yes")}
          />
          <KeyboardArrowDownIcon
            color="info"
            fontSize="small"
            onClick={() => handleChangeFields("No")}
          />
        </Styled.BooleanActionsBlock>
      </Styled.BooleanInputBlock>
    </Styled.BooleanSelectWrapper>
  );
};

export default BooleanSelect;
