import { useState, useEffect, useRef } from "react";
import { useFormikContext } from "formik";
import get from "lodash/get";
import CheckCircle from "shared/icons/checkCircle.svg";
import BlueArrowRigth from "shared/icons/blueArrowRigth.svg";
import InputText from "../../InputText";
import { mappedCoinsForChoice } from "../lib/mappedCoinsForChoice";
import * as Styled from "./index.styled";

type IInputSelectProps = {
  name: string;
  label: string;
};

const InputSelect = (props: IInputSelectProps) => {
  const { name, label } = props;
  const { values, setFieldValue } = useFormikContext();
  const value = get(values, name, [""]);

  const [coins, setCoins] = useState(mappedCoinsForChoice({ value, searchValue: "" }));
  const [leftPositions, setLeftPositions] = useState("");
  const selectBlockRef = useRef();

  const [isOpenMenu, setIsOpenMenu] = useState(false);

  const handleClick = () => {
    const selectBlock = selectBlockRef.current as HTMLDivElement;
    setLeftPositions(selectBlock.offsetLeft + "px");
    setIsOpenMenu(true);
  };

  const handleClose = () => {
    setCoins(mappedCoinsForChoice({ value, searchValue: "" }));
    setIsOpenMenu(false);
  };

  const handleChangeFields = (choiceCoin: string, isSelected: boolean) => {
    if (isSelected) {
      setFieldValue(
        name,
        value.filter((x) => x !== choiceCoin)
      );
    }
    if (!isSelected) {
      setFieldValue(name, [...value, choiceCoin]);
    }
    setCoins((prev) =>
      prev.map(({ coin, isSelected }) => {
        if (coin === choiceCoin) {
          return { coin, isSelected: !isSelected };
        }
        return { coin, isSelected };
      })
    );
  };

  const handleChange = (searchValue: string) => {
    setCoins(() => mappedCoinsForChoice({ value, searchValue }));
  };

  const handleClickWidow = (ev) => {
    const clickedElement = ev.target as HTMLDivElement;
    const selectMenu = document.getElementById("select-menu")!;
    const its_menu = clickedElement == selectMenu || selectMenu.contains(clickedElement);
    if (!its_menu) {
      handleClose();
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickWidow);
    return () => document.removeEventListener("mousedown", handleClickWidow);
  }, []);

  return (
    <Styled.InputSelectWrapper>
      <Styled.InputSelectLabel>{label}</Styled.InputSelectLabel>
      <Styled.SelectBlock onClick={handleClick} ref={selectBlockRef}>
        <Styled.SelectBlockText>{value.join(", ")}</Styled.SelectBlockText>
        <BlueArrowRigth />
      </Styled.SelectBlock>
      {isOpenMenu && (
        <Styled.MenuWrapper id="select-menu" {...{ leftPositions }}>
          <InputText {...{ handleChange }} />
          {coins.map(({ coin, isSelected }) => (
            <Styled.MenuItemBlock onClick={() => handleChangeFields(coin, isSelected)}>
              <p>{coin}</p>
              {isSelected && (
                <Styled.MenuItemIconBlock>
                  <CheckCircle />
                </Styled.MenuItemIconBlock>
              )}
            </Styled.MenuItemBlock>
          ))}
        </Styled.MenuWrapper>
      )}
    </Styled.InputSelectWrapper>
  );
};

export default InputSelect;
