import { coins } from "shared/constants/coins";

type ReturnValue = {
  coin: string;
  isSelected: boolean;
};

type MappedCoinsProps = {
  value: string[];
  searchValue: string | null;
};

export const mappedCoinsForChoice = (props: MappedCoinsProps): ReturnValue[] => {
  const { value, searchValue } = props;
  const mappedCoins = coins.map((coin) => ({ coin, isSelected: value.includes(coin) }));
  const selectCoins = mappedCoins.filter((x) => x.isSelected);
  const nonSelectCoins = mappedCoins.filter((x) => !x.isSelected);
  const res = selectCoins.concat(nonSelectCoins);
  if (searchValue) {
    return res.filter(({ coin }) => coin.includes(searchValue));
  }
  return res;
};
