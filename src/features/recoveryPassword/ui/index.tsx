import { useSearchParams } from "react-router-dom";
import { createPortal } from "react-dom";
import RecoveryUserPasswordModal from "./recoveryUserPasswordModal";

const RecoveryUserPassword = () => {
  const [searchParams] = useSearchParams();
  const recoveryCode = searchParams.get("recoveryCode");

  if (recoveryCode) {
    return createPortal(<RecoveryUserPasswordModal {...{ recoveryCode }} />, document.body);
  }
  return null;
};

export default RecoveryUserPassword;
