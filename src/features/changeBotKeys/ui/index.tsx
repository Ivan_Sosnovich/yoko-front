import { useState, ChangeEvent, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { isMobile } from "react-device-detect";
import { useAppDispatch, useAppSelector } from "providers";
import Button from "shared/ui/Button";
import { botKeysSelectors, changeBotKeysAction, changeKeysIsLoadingSelector } from "../model";
import { IChangeBotKeysData } from "./index.types";
import * as Styled from "./index.styled";

const ChangeBotKeys = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const botKeysData = useAppSelector(botKeysSelectors);
  const isLoading = useAppSelector(changeKeysIsLoadingSelector);

  const [formData, setFormData] = useState<IChangeBotKeysData | null>({
    apiKey: "",
    secretKey: "",
  });

  const handleChange = (name: string, value: string) => {
    setFormData((prev) => ({ ...prev, [name]: value }));
  };

  const handleSubmit = () => {
    dispatch(changeBotKeysAction(formData));
  };

  useEffect(() => {
    if (botKeysData) {
      setFormData(botKeysData);
    }
  }, [botKeysData]);

  return (
    <Styled.ChangeBotKeysWrapper>
      <Styled.TextFieldWrapper>
        <Styled.TextFieldTitle>{t("table.bot.change.keys.title")}</Styled.TextFieldTitle>
        <Styled.TextFieldBlock>
          <Styled.TextFieldLabel htmlFor="apiKey">APl key</Styled.TextFieldLabel>
          <Styled.TextField
            id="apiKey"
            onChange={({ target: { value } }: ChangeEvent<HTMLInputElement>) =>
              handleChange("apiKey", value)
            }
            value={formData["apiKey"]}
          />
        </Styled.TextFieldBlock>
        <Styled.TextFieldBlock>
          <Styled.TextFieldLabel htmlFor="secretKey">Secret key</Styled.TextFieldLabel>
          <Styled.TextField
            id="secretKey"
            onChange={({ target: { value } }: ChangeEvent<HTMLInputElement>) =>
              handleChange("secretKey", value)
            }
            value={formData["secretKey"]}
          />
        </Styled.TextFieldBlock>
      </Styled.TextFieldWrapper>
      <Button
        color="primary"
        size={isMobile ? "small" : "medium"}
        {...{ isLoading }}
        onClick={() => handleSubmit()}
      >
        {t("table.bot.change.keys.button")}
      </Button>
    </Styled.ChangeBotKeysWrapper>
  );
};

export default ChangeBotKeys;
