export type IChangeBotKeysData = {
  apiKey: string;
  secretKey: string;
};
