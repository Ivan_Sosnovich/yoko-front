import styled from "@emotion/styled";

export const ChangeBotKeysWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 40px;

  @media screen and (max-width: 670px) {
    padding: 12px 6px;
    gap: 24px;
  }
`;

export const TextFieldWrapper = styled.div`
  padding: 24px 52px 48px;
  width: 100%;
  height: 236px;
  background: #ffffff;
  border: 2px solid #f5f7fd;
  border-radius: 20px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  gap: 24px;

  @media screen and (max-width: 670px) {
    height: fit-content;
    padding: 10px 10px;
  }
`;

export const TextFieldTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: #181938;

  @media screen and (max-width: 670px) {
    color: #2b3674;
  }
`;

export const TextFieldBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 30px;
  width: 100%;

  @media screen and (max-width: 670px) {
    gap: 10px;
    flex-flow: column;
  }
`;

export const TextFieldLabel = styled.label`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #9298b8;
`;

export const TextField = styled.input`
  padding: 5px 20px;
  width: 85%;
  height: 48px;
  background: #f5f7fd;
  border-radius: 30px;
  outline: none;
  border: none;

  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 0.3px;
  color: #5f5cec;

  @media screen and (max-width: 670px) {
    width: 100%;
  }
`;
