export { default } from "./ui";
export { changeBotKeysAction } from "./model";
export type { IChangeBotKeysData } from "./ui/index.types";
