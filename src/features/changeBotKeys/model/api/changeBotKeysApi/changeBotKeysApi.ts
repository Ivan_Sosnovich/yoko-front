import http from "shared/api/axios";
import { IChangeBotKeysData } from "../../../";

export const changeBotKeysApi = (changeBotKeysDto: IChangeBotKeysData) =>
  http.post<IChangeBotKeysData, IChangeBotKeysData>("bot/settings", changeBotKeysDto);
