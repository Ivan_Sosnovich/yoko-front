import http from "shared/api/axios";
import type { IDepositApi, IDepositResponse } from "./depositApi.types";

export const depositApi = (depositDto: IDepositApi) =>
  http.post<IDepositResponse, IDepositApi>("payment/order/create", depositDto);
