export * from "./depositApi";
export type { IDepositApi, IDepositResponse } from "./depositApi.types";
