import { useState } from "react";
import { useTranslation } from "react-i18next";
import ButtonLib from "@mui/material/Button";
import GreenPlus from "shared/icons/greenPlus.svg";
import UserDepositModal from "./userDepositModal";

const UserDeposit = () => {
  const { t } = useTranslation();

  const [isOpen, setOpenModal] = useState(false);

  const handleOpenDepositModal = () => {
    setOpenModal(true);
  };

  const onClose = () => setOpenModal(false);
  return (
    <>
      <UserDepositModal {...{ onClose, isOpen }} />
      <ButtonLib
        sx={{
          color:
            "#05CD99; text-transform: none; font-style: normal; font-weight: 600; font-size: 14px; line-height: 14px; text-align: right; letter-spacing: 0.3px;",
        }}
        endIcon={<GreenPlus />}
        variant="text"
        size="small"
        onClick={() => handleOpenDepositModal()}
      >
        <p>{t("actions.card.user.main.balance.button.deposit")}</p>
      </ButtonLib>
    </>
  );
};

export default UserDeposit;
