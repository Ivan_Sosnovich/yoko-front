import { useState } from "react";
import ButtonLib from "@mui/material/Button";
import { useTranslation } from "react-i18next";
import ArrowGreen from "shared/icons/arrowGreen.svg";
import TransferReferralBalance from "./transferReferralBalanceModal";

const TransferReferralBalanceModal = () => {
  const { t } = useTranslation();

  const [isOpen, setIsOpen] = useState(false);

  const onClose = () => setIsOpen(false);

  const handleOpenTransferModal = () => setIsOpen(true);

  return (
    <>
      <TransferReferralBalance {...{ isOpen, onClose }} />
      <ButtonLib
        sx={{
          color:
            "#05CD99; text-transform: none; font-style: normal; font-weight: 600; font-size: 14px; line-height: 14px; text-align: right; letter-spacing: 0.3px;",
        }}
        endIcon={<ArrowGreen />}
        variant="text"
        size="small"
        onClick={() => handleOpenTransferModal()}
      >
        <p>{t("referral.card.balance.button.title")}</p>
      </ButtonLib>
    </>
  );
};

export default TransferReferralBalanceModal;
