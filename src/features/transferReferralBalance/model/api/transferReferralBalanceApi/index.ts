import http from "shared/api/axios";
import type { ITransferReferralBalanceDto } from "./index.types";

export const transferReferralBalanceApi = (
  transferReferralBalanceDto: ITransferReferralBalanceDto
) =>
  http.post<null, ITransferReferralBalanceDto>(
    "user/transfer/referral/balance",
    transferReferralBalanceDto
  );

export { ITransferReferralBalanceDto };
