import styled from "@emotion/styled";

export const LogoutWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
  width: 100%;
  height: 100%;
  gap: 60px;
  width: 536px;
  height: 210px;
  padding: 0 42px 0 42px;

  @media screen and (max-width: 800px) {
    gap: 40px;
    width: 301px;
    height: 222px;
  }
`;

export const LogoutText = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;

  text-align: center;
  color: #181938;

  @media screen and (max-width: 800px) {
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 32px;
    text-align: center;
    color: #181938;
  }
`;

export const LogoutActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 15px;

  @media screen and (max-width: 800px) {
    flex-flow: column;
    gap: 10px;
  }
`;
