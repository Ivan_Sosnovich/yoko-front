import { useTranslation } from "react-i18next";
import { useAppDispatch } from "providers";
import { userLogOutAction } from "../../";
import Button from "shared/ui/Button";
import ModalPage from "shared/ui/ModalPage";
import * as Styled from "./index.styled";

interface IUserLogOutModalProps {
  onClose: () => void;
  isOpen: boolean;
}

const UserLogOutModal = (props: IUserLogOutModalProps) => {
  const { onClose, isOpen } = props;
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const handleUserLogOut = () => {
    dispatch(userLogOutAction());
  };

  if (isOpen) {
    return (
      <ModalPage {...{ onClose }}>
        <Styled.LogoutWrapper>
          <Styled.LogoutText>{t("text.logOut")}</Styled.LogoutText>
          <Styled.LogoutActionsWrapper>
            <Button size="medium" variant="contained" color="secondary" onClick={() => onClose()}>
              {t("button.no")}
            </Button>
            <Button
              size="medium"
              variant="contained"
              color="primary"
              onClick={() => handleUserLogOut()}
            >
              {t("button.yes")}
            </Button>
          </Styled.LogoutActionsWrapper>
        </Styled.LogoutWrapper>
      </ModalPage>
    );
  }

  return null;
};

export default UserLogOutModal;
