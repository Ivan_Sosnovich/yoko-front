import AuthModal from "./ui/authModal";
import RegisterModal from "./ui/registerModal";

export { default } from "./ui";
export type { ILoginDto } from "./model/api/loginApi";
export { loginApi } from "./model/api/loginApi";
export { AuthModal, RegisterModal };
export { cleanUseAuthErrorAction } from "./model/actions/cleanUseAuthErrorAction";
export { loginUser } from "./model/actions/loginUserAction";
export type { IRenderComponentProps } from "./ui/index.types";
