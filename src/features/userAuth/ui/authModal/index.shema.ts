import * as Yup from "yup";
import { ILoginDto } from "../../model/api/loginApi";

export type AuthInitialValues = ILoginDto;

export const initialValues: AuthInitialValues = {
  login: "",
  password: "",
};

export const validationSchema = Yup.object().shape({
  login: Yup.string().required("auth.error.login"),
  password: Yup.string().required("auth.error.regiser"),
});

export const recoveryValidationSchema = Yup.object().shape({
  login: Yup.string().required("auth.error.login"),
});

export type RecoveryInitialValues = {
  login: string;
};

export const recoveryInitialValues: RecoveryInitialValues = {
  login: "",
};
