import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Formik } from "formik";
import ButtonLib from "@mui/material/Button";
import { useAppDispatch } from "providers";
import Button from "shared/ui/Button";
import ModalPage from "shared/ui/ModalPage";
import InputField from "shared/ui/InputField";
import SuccessIconBlock from "shared/ui/SuccessIconBlock";
import { loginApi } from "../../model/api/loginApi";
import { loginUser } from "../../model/actions/loginUserAction";
import { cleanUseAuthErrorAction } from "../../model/actions/cleanUseAuthErrorAction";
import { resendEmailForUserApi } from "../../model/api/resendEmailForUser";
import { recoveryPasswordApi } from "../../model/api/recoveryPassword";
import {
  validationSchema,
  initialValues,
  AuthInitialValues,
  recoveryInitialValues,
  recoveryValidationSchema,
  RecoveryInitialValues,
} from "./index.shema";
import * as Styled from "./index.styled";

type IAuthModalProps = {
  onClose: () => void;
  handleChoiseAuthType: () => void;
};

const AuthModal = (props: IAuthModalProps) => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const { onClose, handleChoiseAuthType } = props;

  const [error, setError] = useState<string | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  const [showMainForm, setShowMainForm] = useState(true);
  const [successSendEmail, setSuccessSendEmail] = useState(false);
  const [recoveryPassword, setRecoveryPassword] = useState(false);
  const [successRecoveryPassword, setSuccessRecoveryPassword] = useState(false);

  const handleSubmit = (values: AuthInitialValues) => {
    loginApi(values)
      .then(() => dispatch(loginUser()))
      .catch((e) => setError(e || "Server Error"))
      .finally(() => setIsLoading(false));
  };

  const handleRecovery = ({ login }: RecoveryInitialValues) => {
    setIsLoading(true);
    recoveryPasswordApi({ login })
      .then(() => {
        setSuccessRecoveryPassword(true);
        setRecoveryPassword(false);
      })
      .catch((e) => setError(e))
      .finally(() => setIsLoading(false));
  };

  const handleRecoveryPassword = () => {
    setShowMainForm(false);
    setRecoveryPassword(true);
  };

  const resendEmailForUser = (email: string) => {
    setIsLoading(true);
    resendEmailForUserApi({ email })
      .then(() => {
        setSuccessSendEmail(true);
        setShowMainForm(false);
      })
      .catch((e) => setError(e))
      .finally(() => setIsLoading(false));
  };

  const handleChangeRigisterMode = () => {
    handleChoiseAuthType();
  };

  useEffect(() => {
    return () => {
      dispatch(cleanUseAuthErrorAction());
    };
  }, []);

  return (
    <ModalPage
      onClose={() => {
        setShowMainForm(true);
        setSuccessSendEmail(false);
        setRecoveryPassword(false);
        setSuccessRecoveryPassword(false);
        onClose();
      }}
    >
      <Styled.AuthWrapper>
        {showMainForm && (
          <>
            <Styled.AuthTitle>{t("auth.title")}</Styled.AuthTitle>
            <Formik
              onSubmit={handleSubmit}
              validateOnChange={false}
              {...{ initialValues, validationSchema }}
            >
              {({ errors, handleChange, handleSubmit, values }) => (
                <>
                  <Styled.AuthInputWrapper>
                    <InputField
                      type="text"
                      name="login"
                      placeholder={t("auth.input.login")}
                      handleChange={handleChange}
                    />
                    <InputField
                      type="password"
                      name="password"
                      placeholder={t("auth.input.password")}
                      handleChange={handleChange}
                    />
                    <ButtonLib
                      sx={{ color: "#5F5CEC; text-transform: none;" }}
                      variant="text"
                      size="small"
                      onClick={() => handleRecoveryPassword()}
                    >
                      <p>{t("auth.button.recovery.password")}</p>
                    </ButtonLib>
                  </Styled.AuthInputWrapper>
                  <Styled.AuthInformationBlock>
                    {
                      <Styled.AuthError>
                        {error !== "isActive=false" && t(errors.login || errors.password || error)}
                        {error === "isActive=false" && (
                          <Styled.AuthChangeModeForRegister>
                            {t("auth.error.not.confirmed.your.eMail")}
                            <ButtonLib
                              sx={{ color: "#5F5CEC; text-transform: none;" }}
                              variant="text"
                              size="small"
                              onClick={() => resendEmailForUser(values.login)}
                            >
                              {t("auth.error.resend.email.for.activete")}
                            </ButtonLib>
                          </Styled.AuthChangeModeForRegister>
                        )}
                      </Styled.AuthError>
                    }
                    <Styled.AuthChangeModeForRegister>
                      {t("auth.button.register.account")}
                      <ButtonLib
                        sx={{ color: "#5F5CEC; text-transform: none;" }}
                        variant="text"
                        size="small"
                        onClick={() => handleChangeRigisterMode()}
                      >
                        {t("button.reg")}
                      </ButtonLib>
                    </Styled.AuthChangeModeForRegister>
                    <Button
                      size="medium"
                      color="primary"
                      isLoading={isLoading}
                      {...{ isLoading }}
                      onClick={() => handleSubmit()}
                    >
                      {t("auth.button.submit")}
                    </Button>
                  </Styled.AuthInformationBlock>
                </>
              )}
            </Formik>
          </>
        )}
        {(successSendEmail || successRecoveryPassword) && (
          <Styled.AuthSuccsessWrapper>
            <SuccessIconBlock />
            <Styled.AuthSuccsessTitle>
              {t("auth.information.send.email.title")}
            </Styled.AuthSuccsessTitle>
            <Styled.AuthSuccsessDescription>
              {t("auth.information.send.email.description")}
            </Styled.AuthSuccsessDescription>
          </Styled.AuthSuccsessWrapper>
        )}
        {recoveryPassword && (
          <Styled.AuthInformationBlock>
            <Styled.AuthTitle>{t("auth.recovery.title")}</Styled.AuthTitle>
            <Formik
              onSubmit={handleRecovery}
              validateOnChange={false}
              initialValues={recoveryInitialValues}
              validationSchema={recoveryValidationSchema}
            >
              {({ handleChange, handleSubmit }) => (
                <>
                  <InputField
                    type="login"
                    name="login"
                    placeholder={t("auth.input.login")}
                    handleChange={handleChange}
                  />
                  <Button
                    size="medium"
                    color="primary"
                    onClick={() => handleSubmit()}
                    {...{ isLoading }}
                  >
                    {t("auth.button.recovery.submit")}
                  </Button>
                </>
              )}
            </Formik>
          </Styled.AuthInformationBlock>
        )}
      </Styled.AuthWrapper>
    </ModalPage>
  );
};

export default AuthModal;
