import * as Yup from "yup";
import type { IRegisterDto } from "features/userAuth/model/api/registerApi";

export type RegisterInitialValues = IRegisterDto;

export const initialValues: RegisterInitialValues = {
  name: "",
  email: "",
  password: "",
  passwordRetry: "",
  referralCode: "",
};

export const validationSchema = Yup.object().shape({
  name: Yup.string().required("register.error.login"),
  email: Yup.string().required("register.error.email"),
  password: Yup.string().required("auth.error.regiser"),
  passwordRetry: Yup.string()
    .required("recovery.error")
    .test("validate password", "recovery.error", function (passwordRepeat) {
      const { password } = this.parent;
      return password === passwordRepeat;
    }),
  referralCode: Yup.string().test("validate Ref code", "register.error.refCode", function (code) {
    if (code?.length !== 5 && code?.length > 0) return false;
    return true;
  }),
});
