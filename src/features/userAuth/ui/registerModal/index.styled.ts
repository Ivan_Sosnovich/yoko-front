import styled from "@emotion/styled";

export const RegisterWrapper = styled.div`
  padding: 0 100px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  width: 100%;

  @media screen and (max-width: 670px) {
    padding: 0 16px;
  }
`;

export const RegisterTitle = styled.h3`
  margin-bottom: 40px;
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 56px;
  text-align: center;
  letter-spacing: 0.3px;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 32px;
  }
`;

export const RegisterInputWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  justify-content: flex-start;
  width: 100%;
  gap: 10px;
`;

export const RegisterInformationBlock = styled.div`
  margin-top: 40px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  gap: 40px;
`;

export const RegisterError = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 14px;
  color: #f87c92;
  min-height: 20px;
`;

export const RegisterChangeModeForRegister = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 14px;
  color: #181938;
`;

export const RegisterSuccsessWrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  gap: 24px;
`;

export const RegisterSuccsessTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 40px;
  line-height: 48px;
  text-align: center;
  color: #181938;

  @media screen and (max-width: 670px) {
    font-size: 30px;
    line-height: 32px;
  }
`;

export const RegisterSuccsessDescription = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  letter-spacing: 0.3px;
  color: #181938;
`;
