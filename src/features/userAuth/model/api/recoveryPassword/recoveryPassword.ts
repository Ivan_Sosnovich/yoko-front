import http from "shared/api/axios";
import type { IRecoveryPassword } from "./recoveryPassword.types";

export const recoveryPasswordApi = ({ login }: IRecoveryPassword) =>
  http.get(`user/password/recovery/${login}`);
