export type IResendEmailParams = {
  email: string;
};
