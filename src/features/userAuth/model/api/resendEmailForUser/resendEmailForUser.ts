import http from "shared/api/axios";
import type { IResendEmailParams } from "./resendEmailForUser.types";

export const resendEmailForUserApi = ({ email }: IResendEmailParams) =>
  http.get(`/user/confirmation/resend/${email}`);
