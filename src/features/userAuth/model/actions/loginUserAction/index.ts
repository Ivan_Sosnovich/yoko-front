import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "providers";
import { loginApi, ILoginDto } from "../../api/loginApi";
import { getUserApi, IUser } from "shared/api/user/getUser";

export const loginUser = createAsyncThunk<IUser, undefined, AsyncThunkConfig>(
  "user/login",
  async (_, { rejectWithValue }) => {
    try {
      return await getUserApi();
    } catch (e: any) {
      return rejectWithValue(e);
    }
  }
);
