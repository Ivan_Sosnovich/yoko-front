import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import SuccessIconBlock from "shared/ui/SuccessIconBlock";
import ErrorIconBlock from "shared/ui/ErrorIconBlock";
import ModalPage from "shared/ui/ModalPage";
import { confirmationUserApi } from "../../model/api/confirmationUserApi";
import * as Styled from "./index.styled";

interface IConfirmationUserModalProps {
  code: string;
}

const ConfirmationUserModal = (props: IConfirmationUserModalProps) => {
  const { code } = props;
  const { t } = useTranslation();

  const [open, setIsOpen] = useState<boolean | null>(!!code);
  const [isSuccses, setIsSuccses] = useState(false);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    confirmationUserApi({ code })
      .then(() => setIsSuccses(true))
      .catch((error) => setError(error || "Server error"));
  }, []);

  const onClose = () => {
    setIsOpen(false);
  };

  if (open) {
    return (
      <ModalPage {...{ onClose }}>
        <Styled.ConfirmationUserWrapper>
          {isSuccses && (
            <>
              <SuccessIconBlock />
              <Styled.ConfirmationUserTitle>{t("confirmation.title")}</Styled.ConfirmationUserTitle>
            </>
          )}
          {error && (
            <>
              <ErrorIconBlock />
              <Styled.ConfirmationUserTitle>
                {t("table.deposit.status.error")}
              </Styled.ConfirmationUserTitle>
              <Styled.ConfirmationUserError>{error}</Styled.ConfirmationUserError>
            </>
          )}
        </Styled.ConfirmationUserWrapper>
      </ModalPage>
    );
  }

  return null;
};

export default ConfirmationUserModal;
