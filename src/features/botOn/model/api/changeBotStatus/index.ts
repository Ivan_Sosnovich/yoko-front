import http from "shared/api/axios";
import type { IChangeBotStatusDto } from "./index.types";

export const changeBotStatusApi = (changeBotStatusDto: IChangeBotStatusDto) =>
  http.post<IChangeBotStatusDto, IChangeBotStatusDto>("bot/settings/status", changeBotStatusDto);
