import BotSettingsWidget from "widgets/botSettings";

const HomePage = () => {
  return <BotSettingsWidget />;
};

export default HomePage;
