import PaymentWidget from "widgets/payment";

const PaymentPage = () => {
  return <PaymentWidget />;
};

export default PaymentPage;
