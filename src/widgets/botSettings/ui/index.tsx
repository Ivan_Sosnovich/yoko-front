import HomePageWrapper from "entities/HomePageWrapper";
import { botSettingsData } from "../config";

const BotSettings = () => {
  return <HomePageWrapper {...botSettingsData} />;
};

export default BotSettings;
