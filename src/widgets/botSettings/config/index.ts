import { CardsType, IHomePageWrapperProps } from "entities/HomePageWrapper";

export const botSettingsData: IHomePageWrapperProps = {
  title: "text.home",
  tableType: "BOT_SETTINGS",
  cards: [CardsType.USER_MAIN_BALANCE, CardsType.BOT_ACTIONS, CardsType.TRADING_BALANCE],
};
