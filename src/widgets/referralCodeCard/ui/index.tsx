import ReduceCapacityIcon from "@mui/icons-material/ReduceCapacity";
import { useAppSelector } from "providers";
import ActionsCard from "shared/ui/actionsCard";
import { getUserReferralCodeSelector } from "../model/selectors/getUserReferralCodeSelector";

const ReferralCodeCard = () => {
  const referralCode = useAppSelector(getUserReferralCodeSelector)!;

  return (
    <ActionsCard
      Icon={ReduceCapacityIcon}
      title="referral.card.code.title"
      description={referralCode}
      descriptionColor="#5F5CEC"
      info="referral.card.code.info"
    />
  );
};

export default ReferralCodeCard;
