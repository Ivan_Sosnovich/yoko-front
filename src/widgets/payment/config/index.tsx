import { IHomePageWrapperProps } from "entities/HomePageWrapper";

export const paymentData: IHomePageWrapperProps = {
  title: "text.history.deposits",
  tableType: "PAYMENT",
};
