import HomePageWrapper from "entities/HomePageWrapper";
import { paymentData } from "../config";

const Payment = () => {
  return <HomePageWrapper {...paymentData} />;
};

export default Payment;
