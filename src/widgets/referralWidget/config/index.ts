import { CardsType, IHomePageWrapperProps } from "entities/HomePageWrapper";

export const referralData: IHomePageWrapperProps = {
  title: "text.referral",
  tableType: "REFERRAL",
  cards: [CardsType.REFERRAL_BALANCE, CardsType.REFERRAL_COUNT, CardsType.REFERRAL_CODE],
};
