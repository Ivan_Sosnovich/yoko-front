import HomePageWrapper from "entities/HomePageWrapper";
import { referralData } from "../config";

const ReferralWidget = () => {
  return <HomePageWrapper {...referralData} />;
};

export default ReferralWidget;
