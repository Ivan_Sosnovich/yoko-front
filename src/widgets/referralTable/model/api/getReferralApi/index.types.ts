export type IReferral = {
  id: number;
  referralUser: string;
  profit: number;
  date: string;
};

export type IReferralData = {
  data: IReferral[];
  count: number;
};

export type IGetReferralParams = {
  pageNumber: number;
  count: number;
};
