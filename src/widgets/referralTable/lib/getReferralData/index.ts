import { ITableProps } from "shared/ui/Table";
import { getFormatDate } from "shared/lib/getFormatDate";
import { IReferralData } from "../../model/api/getReferralApi";

export const getReferralData = (
  referralData: IReferralData
): Pick<ITableProps, "headCells" | "rows" | "pageCount"> => {
  const { count, data } = referralData;

  const headCells = [
    {
      width: "33.3%",
      content: "table.referral.date",
      position: "left",
      flexPosition: "flex-start",
    },
    {
      width: "33.3%",
      content: "table.referral.name",
      position: "center",
      flexPosition: "center",
    },
    {
      width: "33.3%",
      content: "table.referral.profit",
      position: "right",
      flexPosition: "flex-end",
    },
  ];

  const rows: ITableProps["rows"] = data.map(({ date, referralUser, profit }) => {
    return {
      mobileCard: {
        title: referralUser,
        description: getFormatDate(date),
        descriptionBlock: {
          type: "table",
          tableCell: {
            type: "text",
            title: "table.referral.card.profit",
            value: `${profit} USDT`,
            position: "center",
            isNotTranslate: true,
          },
        },
      },
      cells: [
        {
          width: "33.3%",
          content: getFormatDate(date),
          position: "left",
          type: "text",
          flexPosition: "flex-start",
          isNotTranslate: true,
        },
        {
          width: "33.3%",
          content: referralUser,
          position: "center",
          type: "text",
          flexPosition: "center",
          isNotTranslate: true,
        },
        {
          width: "33.3%",
          content: `${profit} USDT`,
          position: "right",
          type: "text",
          flexPosition: "flex-end",
          isNotTranslate: true,
        },
      ],
    };
  });

  return {
    pageCount: Math.ceil(count / 10),
    headCells,
    rows,
  };
};
