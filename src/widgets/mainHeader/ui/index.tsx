import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import LanguageSelectionMenu from "entities/languageSelectionMenu";
import Logo from "entities/logo";
import AuthButton from "./authButton";
import MenuBlock from "./menuBlock";
import * as Styled from "./index.styled";

const MainHeader = () => {
  const { t } = useTranslation();

  return (
    <Styled.MainHeaderWrapper>
      <Styled.MainHeaderContentWrapper>
        <Logo type="home" />
        <Styled.MainHeaderLinkBlock>
          <Styled.MainHeaderLink>{t("link.profit")}</Styled.MainHeaderLink>
          <Styled.MainHeaderLink>{t("link.robot")}</Styled.MainHeaderLink>
          <Styled.MainHeaderLink>{t("link.referral_program")}</Styled.MainHeaderLink>
          <Styled.MainHeaderLink>{t("link.questions")}</Styled.MainHeaderLink>
          <Styled.MainHeaderLink>{t("link.about")}</Styled.MainHeaderLink>
        </Styled.MainHeaderLinkBlock>
        <Styled.MainHeaderActionsBlock>
          <Styled.MainHeaderRegisterButton>
            <UserAuth type="auth" renderComponent={(props) => <AuthButton {...props} />} />
          </Styled.MainHeaderRegisterButton>
          <LanguageSelectionMenu />
          <MenuBlock />
        </Styled.MainHeaderActionsBlock>
      </Styled.MainHeaderContentWrapper>
    </Styled.MainHeaderWrapper>
  );
};

export default MainHeader;
