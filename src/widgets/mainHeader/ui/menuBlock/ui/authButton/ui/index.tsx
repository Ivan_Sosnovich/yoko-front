import { useTranslation } from "react-i18next";
import { IRenderComponentProps } from "features/userAuth";
import Button from "shared/ui/Button/Button";

const AuthButton = (props: IRenderComponentProps) => {
  const { handleClick } = props;
  const { t } = useTranslation();

  return (
    <Button size="small" variant="contained" color="primary" onClick={() => handleClick()}>
      {t("button.auth")}
    </Button>
  );
};

export default AuthButton;
