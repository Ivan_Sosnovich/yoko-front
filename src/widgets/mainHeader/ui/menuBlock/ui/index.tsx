import { useState } from "react";
import { useTranslation } from "react-i18next";
import Drawer from "@mui/material/Drawer";
import MenuIcon from "@mui/icons-material/Menu";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import UserAuth from "features/userAuth";
import AuthButton from "./authButton";
import RegisterButton from "./registerButton";
import { headerMenuItems } from "../config";
import * as Styled from "./index.styled";

const MenuBlock = () => {
  const { t } = useTranslation();

  const [openMenu, setOpenMenu] = useState(false);

  const handleOpenMenu = () => {
    setOpenMenu((prev) => !prev);
  };

  return (
    <Styled.MenuBlockWrapper>
      <Styled.MenuIconButton onClick={handleOpenMenu}>
        <MenuIcon sx={{ color: "#5F5CEC" }} />
      </Styled.MenuIconButton>
      <Drawer anchor="right" open={openMenu} onClose={() => handleOpenMenu()}>
        <Styled.MenuBlockContent direction="column" spacing={2}>
          <List>
            {headerMenuItems.map(({ title }) => (
              <ListItem sx={{ width: "286px" }}>
                <ListItemButton>
                  <ListItemText primary={t(title)} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          <Styled.MenuBlockActions>
            {/* <SocialIconBlock color="#9298B8" /> */}
            <Styled.MenuBlockAuthBlock>
              <UserAuth type="auth" renderComponent={(props) => <AuthButton {...props} />} />
              <UserAuth
                type="register"
                renderComponent={(props) => <RegisterButton {...props} />}
              />
            </Styled.MenuBlockAuthBlock>
          </Styled.MenuBlockActions>
        </Styled.MenuBlockContent>
      </Drawer>
    </Styled.MenuBlockWrapper>
  );
};

export default MenuBlock;
