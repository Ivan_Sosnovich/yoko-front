import styled from "@emotion/styled";
import Stack from "@mui/material/Stack";

export const MenuBlockWrapper = styled.div`
  margin-left: 0 !important;
  display: flex;
  align-items: center;
  justify-items: center;

  @media screen and (min-width: 1276px) {
    display: none;
  }
`;

export const MenuIconButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const MenuBlockContent = styled(Stack)`
  margin-left: 0 !important;
  align-content: space-between;
  justify-content: space-between;
  height: 100%;
`;

export const MenuBlockActions = styled.div`
  display: flex;
  align-items: center;
  justify-items: center;
  flex-flow: column nowrap;
  margin-bottom: 20px !important;
  gap: 20px;
`;

export const MenuBlockAuthBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 10px;

  @media screen and (min-width: 1024px) {
    display: none;
  }
`;
