import { IRenderComponentProps } from "features/userAuth";
import { useTranslation } from "react-i18next";
import UserIcon from "shared/icons/UserIcon.svg";
import Button from "shared/ui/Button/Button";
import * as Styled from "./index.styled";

const AuthButton = (props: IRenderComponentProps) => {
  const { handleClick } = props;
  const { t } = useTranslation();

  return (
    <Button size="medium" onClick={() => handleClick()} color="secondary">
      <Styled.AuthButtonWrapper>
        <Styled.AuthButtonIconBlock>
          <UserIcon />
        </Styled.AuthButtonIconBlock>
        <Styled.AuthButtonText>{t("button.register")}</Styled.AuthButtonText>
      </Styled.AuthButtonWrapper>
    </Button>
  );
};

export default AuthButton;
