import styled from "@emotion/styled";

export const MainHeaderWrapper = styled.div`
  width: 100vw;
  height: 70px;
  left: 0px;
  top: 0px;
  background: #ffffff;
  box-shadow: 0px 25px 20px -25px rgba(20, 25, 143, 0.1);
`;

export const MainHeaderContentWrapper = styled.div`
  margin: 0 auto;
  padding: 12px 40px;
  max-width: 1400px;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const MainHeaderLinkBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 50px;
`;

export const MainHeaderLink = styled.a`
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  line-height: 14px;
  color: #181938;
  text-decoration: none;
  cursor: pointer;

  @media screen and (max-width: 1276px) {
    display: none;
  }
`;

export const MainHeaderActionsBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 30px;
`;

export const MainHeaderRegisterButton = styled.div`
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;
