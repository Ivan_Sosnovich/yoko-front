import WalletIcon from "@mui/icons-material/Wallet";
import { useAppSelector } from "providers";
import TransferReferralBalance from "features/transferReferralBalance";
import ActionsCard from "shared/ui/actionsCard";
import { getUserReferalBalanceSelector } from "../model/selectors/getUserReferalBalanceSelector";

const ReferralBalanceCard = () => {
  const referralBalance = useAppSelector(getUserReferalBalanceSelector)!;

  return (
    <ActionsCard
      Icon={WalletIcon}
      title="referral.card.balance.title"
      description={
        Number.isInteger(referralBalance)
          ? referralBalance + " USDT"
          : referralBalance.toFixed(6) + " USDT"
      }
      descriptionColor={referralBalance > 0 ? "#05CD99" : "#E9002A"}
      Action={TransferReferralBalance}
    />
  );
};

export default ReferralBalanceCard;
