import { IStore } from "providers";

export const getUserReferalBalanceSelector = (state: IStore) =>
  state.userReduser.user.referralBalance;
