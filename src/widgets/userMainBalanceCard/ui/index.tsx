import WalletIcon from "@mui/icons-material/Wallet";
import { useAppSelector } from "providers";
import UserDeposit from "features/userDeposit";
import ActionsCard from "shared/ui/actionsCard";
import { getUserBalanceSelector } from "../model/selectors/getUserBalanceSelector";

const UserMainBalanceCard = () => {
  const balance = useAppSelector(getUserBalanceSelector)!;

  return (
    <ActionsCard
      Icon={WalletIcon}
      title="actions.card.user.main.balance.title"
      description={Number.isInteger(balance) ? balance + " USDT" : balance.toFixed(6) + " USDT"}
      descriptionColor={balance > 0 ? "#05CD99" : "#E9002A"}
      Action={UserDeposit}
    />
  );
};

export default UserMainBalanceCard;
