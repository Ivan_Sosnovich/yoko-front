import { IDepositStatus } from "../../model/api/getDepositApi";

export const getStatusText = (status: IDepositStatus) => {
  switch (status) {
    case "init":
      return "table.deposit.status.init";
    case "error":
      return "table.deposit.status.error";
    case "processed":
      return "table.deposit.status.processed";
    case "pending":
      return "table.deposit.status.pending";
    case "expired":
      return "table.deposit.status.expired";
    case "partial":
      return "table.deposit.status.partial";
    default:
      return "table.deposit.status.init";
  }
};
