import { createPortal } from "react-dom";
import { useTranslation } from "react-i18next";
import ModalPage from "shared/ui/ModalPage";
import Table from "shared/ui/Table";
import { getTableData } from "../lib/getTableData";
import type { IDepositModalProps } from "./index.types";
import * as Styled from "./index.styled";

const DepositModal = (props: IDepositModalProps) => {
  const { t } = useTranslation();
  const { transactionsIds, onClose, depositId } = props;

  if (!transactionsIds?.length) return null;

  const tableData = getTableData(transactionsIds);

  return createPortal(
    <ModalPage {...{ onClose }}>
      <Styled.DepositModalWrapper>
        <Styled.DepositModalInformationBlock>
          <Styled.DepositModalInformationTitle>
            {t("table.transactions.title")}
          </Styled.DepositModalInformationTitle>
          <Styled.DepositModalInformationDescription>{`№ ${depositId}`}</Styled.DepositModalInformationDescription>
        </Styled.DepositModalInformationBlock>
        <Table {...tableData} />
      </Styled.DepositModalWrapper>
    </ModalPage>,
    document.body
  );
};

export default DepositModal;
