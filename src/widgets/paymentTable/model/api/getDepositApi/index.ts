import http from "shared/api/axios";
import { GET_USER_DEPOSIT_PATH } from "shared/api/path";
import type {
  IGetDepositParams,
  IDepositData,
  ITransactions,
  IDepositStatus,
  IDeposit,
} from "./index.types";

export const getDepositApi = (params: IGetDepositParams): Promise<IDepositData> => {
  const { pageNumber, count } = params;
  return http.get<IDepositData>(`${GET_USER_DEPOSIT_PATH}/${count}/${pageNumber}`);
};

export type { IGetDepositParams, IDepositData, ITransactions, IDepositStatus, IDeposit };
