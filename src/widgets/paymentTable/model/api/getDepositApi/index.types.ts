export type IDepositStatus = "init" | "error" | "processed" | "pending" | "expired" | "partial";

export type ITransactions = {
  id: number;
  createAt: string;
  currency: string;
  amount: string;
};

export type IDeposit = {
  id: number;
  status: IDepositStatus;
  amount: string;
  createAt: string;
  received?: string;
  transactionsIds?: ITransactions[];
  link?: string;
};

export type IDepositData = {
  data: IDeposit[];
  count: number;
};

export type IGetDepositParams = {
  pageNumber: number;
  count: number;
};
