import { IDeposit, IDepositData } from "./index.types";
const mockDepositData: IDeposit[] = [
  {
    id: 0,
    status: "expired",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
  {
    id: 1,
    status: "pending",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
  {
    id: 2,
    status: "partial",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
  {
    id: 3,
    status: "processed",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
  {
    id: 4,
    status: "processed",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
  {
    id: 5,
    status: "partial",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
    transactionsIds: [
      {
        id: 0,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 1,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 2,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 3,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 4,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 5,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
      {
        id: 6,
        createAt: "23.04.2022 в 15:47",
        currency: "USDT",
        amount: "9.71",
      },
    ],
  },
  {
    id: 6,
    status: "pending",
    amount: "456",
    createAt: "23.04.2022 в 15:47",
    received: "112356",
    link: "https://api.test.yokotrade.io/api/swagger#/user%20controller/UsersController_getDepositUserData",
  },
];

export const mockDeposit: IDepositData = {
  data: mockDepositData,
  count: 4,
};
