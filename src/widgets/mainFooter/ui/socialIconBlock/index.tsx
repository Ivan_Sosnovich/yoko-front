import { IconButton } from "@mui/material";
import { openInstagramLink } from "shared/lib/openInstagramLink";
import { openTelegramLink } from "shared/lib/openTelegramLink";
import TelegramIcon from "./icons/Telegram.svg";
import InstagramIcon from "./icons/Instagram.svg";
import VKIcon from "./icons/VK.svg";
import TwitterIcon from "./icons/Twitter.svg";
import * as Styled from "./index.styled";

const SocialIconBlock = () => {
  return (
    <Styled.FooterSocialBlock>
      <IconButton>
        <TwitterIcon />
      </IconButton>
      <IconButton onClick={() => openTelegramLink()}>
        <TelegramIcon />
      </IconButton>
      <IconButton>
        <VKIcon />
      </IconButton>
      <IconButton onClick={() => openInstagramLink()}>
        <InstagramIcon />
      </IconButton>
    </Styled.FooterSocialBlock>
  );
};

export default SocialIconBlock;
