import { useTranslation } from "react-i18next";
import Logo from "entities/logo";
import SocialBlock from "./socialIconBlock";
import * as Styled from "./index.styled";

const MainFooter = () => {
  const { t } = useTranslation();

  return (
    <Styled.FooterWrapper>
      <Logo type="main" />
      <Styled.PoliticsBlock>
        <Styled.MainFooterText>{t("text.terms_of_use")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("text.privacy_policy")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("text.cookie_policy")}</Styled.MainFooterText>
      </Styled.PoliticsBlock>
      <Styled.SectionsBlock>
        <Styled.MainFooterText>{t("link.profit")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("link.robot")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("link.referral_program")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("link.questions")}</Styled.MainFooterText>
        <Styled.MainFooterText>{t("link.about")}</Styled.MainFooterText>
      </Styled.SectionsBlock>
      <SocialBlock />
    </Styled.FooterWrapper>
  );
};

export default MainFooter;
