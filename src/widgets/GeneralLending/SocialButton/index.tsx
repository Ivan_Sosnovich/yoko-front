import { openTelegramLink } from "shared/lib/openTelegramLink";
import SmallTelegramButton from "./icons/SmallTelegramButton.svg";
import * as Styled from "./index.styled";

const SocialButton = () => {
  return (
    <Styled.SocialButton onClick={() => openTelegramLink()}>
      <SmallTelegramButton />
    </Styled.SocialButton>
  );
};

export default SocialButton;
