import styled from "@emotion/styled";

export const SocialButton = styled.div`
  position: fixed;
  right: 72px;
  bottom: 72px;
  width: 80px;
  height: 80px;
  background: #5f5cec;
  box-shadow: 0px 4px 96px rgba(95, 92, 236, 0.6), inset 0px 4px 7px rgba(255, 255, 255, 0.25);
  border-radius: 50%;

  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: 100;

  @media screen and (max-width: 670px) {
    width: 72px;
    height: 72px;
    right: 20px;
    bottom: 20px;
    box-shadow: 0px 2.4px 57.6px rgba(95, 92, 236, 0.6),
      inset 0px 2.4px 4.2px rgba(255, 255, 255, 0.25);
    border-radius: 50%;

  }
`;
