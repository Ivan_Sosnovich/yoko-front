import { useTranslation } from "react-i18next";
import NewRobotImagePath from "./assets/img/newrobot.svg";

const CardChoiceBlog = () => {
  const { t } = useTranslation();
  return (
    <div className="block_item blog__item">
      <div className="block_item_image">
        <NewRobotImagePath />
      </div>
      <div className="block_item_title blog__item-title">
        {t("main.choice.best.cryptocurrency.trading")}
      </div>
      <div className="block_item_description">
        {t("main.choice.cryptocurrency.changing.market.change.ratings.changes.version.article")}
      </div>
      <div className="blog__button">{t("main.choice.more.detailed")}</div>
    </div>
  );
};

export default CardChoiceBlog;
