import { SliderSettings } from "shared/ui/Slider/Slider.types";
import { Navigation, Pagination } from "swiper";

export const settingSliderBlog: SliderSettings = {
  modules: [Navigation, Pagination],
  navigation: {
    nextEl: ".blog-arrow-next",
    prevEl: ".blog-arrow-prev",
  },
  pagination: {
    el: ".paginationWrapperBlog",
    clickable: true,
  },
  grabCursor: true,
  slidesPerView: 1.1,
  spaceBetween: 10,
  loop: true,
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    1024: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    1280: {
      slidesPerView: 3,
      spaceBetween: 30,
    },
  },
};
