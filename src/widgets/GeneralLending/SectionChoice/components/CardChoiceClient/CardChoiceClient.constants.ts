import { SliderSettings } from "shared/ui/Slider/Slider.types";
import { Navigation, Pagination } from "swiper";

export const cardChoiceClientItem: CardChoiceClientItemTypes = [
  "Внезапно, сделанные на базе интернет-аналитики выводы и по сей день остаются уделом либералов, которые" +
    " жаждут быть объявлены нарушающими общечеловеческие нормы этики и морали. Современные технологии достигли такого уровня, " +
    "что понимание сути ресурсосберегающих технологий создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий.",
  "Внезапно, сделанные на базе интернет-аналитики выводы и по сей день остаются уделом либералов, которые" +
    " жаждут быть объявлены нарушающими общечеловеческие нормы этики и морали. Современные технологии достигли такого уровня, " +
    "что понимание сути ресурсосберегающих технологий создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий.",
];

type CardChoiceClientItemTypes = Array<string>;

export const settingSliderClient: SliderSettings = {
  modules: [Navigation, Pagination],
  navigation: {
    nextEl: ".client-arrow-next",
    prevEl: ".client-arrow-prev",
  },
  pagination: {
    el: ".paginationWrapperClient",
    clickable: true,
  },
  grabCursor: true,
  slidesPerView: 1.1,
  spaceBetween: 10,
  loop: true,
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    1024: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    1280: {
      slidesPerView: 2,
      spaceBetween: 60,
    },
  },
};
