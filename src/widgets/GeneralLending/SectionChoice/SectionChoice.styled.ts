import styled from "@emotion/styled";

export const WrapperSlider = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 30px;
  margin-bottom: 200px;

  .blog-arrow-prev,
  .blog-arrow-next {
    position: relative;
    padding: 10px;
    background: #ffffff;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .client-arrow-prev,
  .client-arrow-next {
    background: #f4f7fe;
    padding: 10px;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  @media (max-width: 480px) {
    .blog-arrow-prev,
    .client-arrow-prev {
      display: none;
    }

    margin-bottom: 100px;
    .dotted-block-blog,
    .dotted-block-client {
      position: absolute;
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      align-items: center;
      right: 15px;
      bottom: -50px;
    }

    .dotted-block-blog {
      bottom: -50px;
    }
  }
  @media (max-width: 1024px) {
    gap: 20px;
  }

  .wrapper-pagination-blog,
  .wrapper-pagination-client {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }
`;
export const PaginationWrapper = styled.div`
  display: none;
  @media (max-width: 480px) {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    .swiper-pagination-bullet {
      display: none;
      width: 10px;
      height: 3px;
      opacity: 1;
      background: rgba(0, 0, 0, 0.2);
    }

    .swiper-pagination-bullet-active {
      background: #5f5cec;
    }
  }
`;
