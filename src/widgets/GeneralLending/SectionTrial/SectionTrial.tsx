import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import RobotBgPath from "shared/image/robot.png";

const SectionTrial = () => {
  const { t } = useTranslation();

  return (
    <section className="trial">
      <div className="container">
        <div className="trial__wrapper">
          <div className="trial__description">
            <h1 className="main-header trial__description_title">{t("main.trial.title")}</h1>
            <div className="main-subheader trial__description_subtitle">
              {t("main.trial.description")}
            </div>
            <div className="trial__bonus">
              {t("main.trial.bonus")} <span className="trial__bonus_span">10 USDT</span>
            </div>
            <UserAuth
              type="auth"
              renderComponent={({ handleClick }) => (
                <div onClick={() => handleClick()} className="button trial__description_button">
                  {t("main.trial.auth.button")}
                </div>
              )}
            />
          </div>
          <div className="trial__description-robot">
            <img src={RobotBgPath} alt="robot" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default SectionTrial;
