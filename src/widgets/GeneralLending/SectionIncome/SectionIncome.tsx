import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import IncomTabletImagePath from "shared/image/IncomTablet.png";
import LoopIcon from "../assets/icons/violet/loop.svg";
import CompIcon from "../assets/icons/violet/comp.svg";
import ClockIcon from "../assets/icons/violet/clock.svg";
import ProtectIcon from "../assets/icons/violet/protect.svg";
import IncomImagePath from "../assets/img/main/income.svg";

const SectionIncome = () => {
  const { t } = useTranslation();

  return (
    <section className="income">
      <div className="container">
        <div className="income__wrapper">
          <h2 className="section-header income__header">
            {t("main.income.create.passive.income.cryptocurrency")}
          </h2>
          <div className="section-subheader income_subtitle">
            {t("main.income.mind.business.smart.yoko.robot.everything.you")}
          </div>
          <div className="income__info-block">
            <div className="income__info-block_item block_item">
              <div className="income__info-block_image block_item_image">
                <LoopIcon />
              </div>
              <div className="income__info-block_item-description">
                <div className="income__info-block_title block_item_title">
                  {t("main.income.coin.analysis")}
                </div>
                <div className="income__info-block_description block_item_description">
                  {t("main.income.robot.nalyzes.charts.selects.successful.coins.trading")}
                </div>
              </div>
            </div>
            <div className="income__info-block_item block_item">
              <div className="income__info-block_image block_item_image">
                <CompIcon />
              </div>
              <div className="income__info-block_item-description">
                <div className="income__info-block_title block_item_title">
                  {t("main.income.buying.selling")}
                </div>
                <div className="income__info-block_description block_item_description">
                  {t("main.income.knows.buy.sell.closes.profitable.trades")}
                </div>
              </div>
            </div>
            <div className="income__info-block_item block_item">
              <div className="income__info-block_image block_item_image">
                <ClockIcon />
              </div>
              <div className="income__info-block_item-description">
                <div className="income__info-block_title block_item_title">
                  {t("main.income.works.24.7")}
                </div>
                <div className="income__info-block_description block_item_description">
                  {t(
                    "main.income.robot.not.need.sleep.works.monitors.situation.breaks.days.while.rest"
                  )}
                </div>
              </div>
            </div>
            <div className="income__info-block_item block_item">
              <div className="income__info-block_image block_item_image">
                <ProtectIcon />
              </div>
              <div className="income__info-block_item-description">
                <div className="income__info-block_title block_item_title">
                  {t("main.income.safety")}
                </div>
                <div className="income__info-block_description block_item_description">
                  {t(
                    "main.income.your.money.your.money.your.stored.exchange.have.access.them.robot.cannot.deposit.withdraw.money.open.close.trades"
                  )}
                </div>
              </div>
            </div>
          </div>
          <UserAuth
            type="auth"
            renderComponent={({ handleClick }) => (
              <div onClick={() => handleClick()} className="button income_button">
                {t("main.income.create.passive.income")}
              </div>
            )}
          />
        </div>
        <div className="income__exchange-block">
          <div className="income__exchange-block_description">
            <h2 className="section-header exchange-block_header">
              {t("main.income.we.work.largest.exchanges")}
            </h2>
            <div className="section-subheader exchange-block_subheader">
              Binance, Huobi, OKX, ByBit
            </div>
          </div>
          <div className="income__exchange-block_image">
            <div className="full-screen income-full-screen">
              <IncomImagePath />
            </div>
            <img src={IncomTabletImagePath} className="tablet-screen income-tablet" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default SectionIncome;
