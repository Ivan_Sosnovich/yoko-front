import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import DeskImagePath from "shared/image/DeskImage.png";

const SectionWatch = () => {
  const { t } = useTranslation();

  return (
    <section className="watch">
      <div className="container watch-container">
        <div className="watch__wrapper">
          <h2 className="section-header watch__header">
            {t("main.watch.cryptocurrencies.hard")}
            <br />
            {t("main.watch.nose")}
            <span className="watch__header_span">{t("main.watch.yoko.botom")}</span>
            {t("main.watch.everything.become.simple.understandable")}
          </h2>
          <div className="section-subheader">{t("main.watch.connect.robot.easy.steps")}</div>
          <div className="watch__subscription-block_wrapper">
            <div className="block_item subscription-block__item">
              <div className="subscription-block__item_image">
                <div className="subscription-block__item-circle reg">
                  <div className="subscription-block__item-num">1</div>
                </div>
              </div>
              <div className="watch__subscription-block_column">
                <div className="block_item_title watch___item_title">
                  {t("main.watch.register.title")}
                </div>
                <div className="block_item_description watch___item_description">
                  {t("main.watch.register.website.get.personal.account")}
                </div>
              </div>
            </div>
            <div className="block_item subscription-block__item">
              <div className="subscription-block__item_image">
                <div className="subscription-block__item-circle account">
                  <div className="subscription-block__item-num">2</div>
                </div>
              </div>
              <div className="watch__subscription-block_column">
                <div className="block_item_title watch___item_title">
                  {t("main.watch.exchange.account")}
                </div>
                <div className="block_item_description watch___item_description">
                  {t("main.watch.account.exchange.register.referral.bonus")}
                </div>
              </div>
            </div>
            <div className="block_item subscription-block__item">
              <div className="subscription-block__item_image">
                <div className="subscription-block__item-circle robo">
                  <div className="subscription-block__item-num">3</div>
                </div>
              </div>
              <div className="watch__subscription-block_column">
                <div className="block_item_title watch___item_title">
                  {t("main.transaction.connect.robot")}
                </div>

                <div className="block_item_description watch___item_description">
                  {t("main.watch.receive.detailed.instructions.connecting.robot.account.exchange")}
                </div>
              </div>
            </div>
            <div className="block_item subscription-block__item">
              <div className="subscription-block__item_image">
                <div className="subscription-block__item-circle money">
                  <div className="subscription-block__item-num">4</div>
                </div>
              </div>
              <div className="watch__subscription-block_column">
                <div className="block_item_title watch___item_title">
                  {t("main.watch.trade.profit")}
                </div>
                <div className="block_item_description watch___item_description">
                  {t("main.watch.robot.trades.follow.transactions.personal.account.earn")}
                </div>
              </div>
            </div>
          </div>
          <UserAuth
            type="auth"
            renderComponent={({ handleClick }) => (
              <div onClick={() => handleClick()} className="button watch_button">
                {t("main.watch.create.account")}
              </div>
            )}
          />
        </div>
        <div className="desk">
          <div className="desk__block">
            <h2 className="section-header desk__block_header">
              {t("main.watch.keep.track.transactions.convenient.personal.account")}
            </h2>
            <div className="section-subheader color-blk desk__block-subheader">
              {t("main.watch.follow.full.statistics.trading.device.wherever")}
            </div>
          </div>
          <img src={DeskImagePath} className="block_image watch-image desk_image-block" />
        </div>
      </div>
    </section>
  );
};

export default SectionWatch;
