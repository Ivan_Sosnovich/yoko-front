import { useTranslation } from "react-i18next";
import UserAuth from "features/userAuth";
import Slider from "shared/ui/Slider";
import { getProfitCards } from "shared/lib/getProfitCards";
import PhoneImagePath from "shared/image/PhoneImage.png";
import CardItemTransaction, { settingSliderTransaction } from "./components/CardItemTransaction";
import ArrowChevronForward2Icon from "../assets/icons/signs/ArrowChevronForward-2.svg";
import ArrowChevronForwardIcon from "../assets/icons/signs/ArrowChevronForward.svg";
import * as Styled from "./SectionTransaction.styled";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

const SectionTransaction = () => {
  const { t } = useTranslation();

  const cardItems = getProfitCards();

  return (
    <section className="transaction">
      <div className="container">
        <div className="connect__wrapper">
          <div className="block_image transaction-block__image">
            <img src={PhoneImagePath} className="transaction-image" />
          </div>
          <div className="connect__block_description">
            <h2 className="section-header connect__block_header">
              {t("main.transaction.we.want.you.earn")}
            </h2>
            <Styled.TransactionsDescription>
              {t("main.transaction.we.connect.robot.absolutely.charge.subscription.fees")}
              <p>
                {t("main.transaction.percentage.profit.calculated.size.deposit.trading.period")}
              </p>
            </Styled.TransactionsDescription>
            <UserAuth
              type="auth"
              renderComponent={({ handleClick }) => (
                <div onClick={() => handleClick()} className="button connect__block_button">
                  {t("main.transaction.connect.robot")}
                </div>
              )}
            />
          </div>
        </div>
      </div>
      <div className="container transaction-container">
        <div className="transaction__wrapper">
          <h2 className="section-header transaction_header">
            {t("main.transaction.last.closed.deals.yoko.robot")}
          </h2>
          <div className="section-subheader transaction_subheader">
            {t("main.transaction.keep.track.latest.robot.trades")}
          </div>
          <Styled.CarouselWrapper>
            <div className="arrow-left">
              <ArrowChevronForward2Icon />
            </div>
            <Slider
              settings={settingSliderTransaction}
              items={cardItems.map((item) => (
                <CardItemTransaction
                  sum={item.sum}
                  currency={item.currency}
                  exchange={item.exchange}
                  date={item.date}
                />
              ))}
            />
            <div className="dotted-block">
              <div className="arrow-right">
                <ArrowChevronForwardIcon />
              </div>

              <Styled.PaginationWrapper className="paginationWrapperTransaction" />
            </div>
          </Styled.CarouselWrapper>
        </div>
      </div>
    </section>
  );
};

export default SectionTransaction;
