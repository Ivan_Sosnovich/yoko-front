import styled from "@emotion/styled";

export const CarouselWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  width: 100%;
  margin: 55px 0;
  padding-bottom: 20px;
  gap: 30px;

  .arrow-left,
  .arrow-right {
    position: relative;
    padding: 10px;
    background: #ffffff;
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  @media (max-width: 480px) {
    .arrow-left {
      display: none;
    }

    .arrow-right {
      bottom: 0;
    }
    .dotted-block {
      display: flex;
      flex-flow: row nowrap;
      justify-content: flex-end;
      align-items: center;
      position: absolute;
      bottom: -15px;
      right: 0;
    }
  }
`;
export const PaginationWrapper = styled.div`
  display: none;
  @media (max-width: 480px) {
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: center;

    .swiper-pagination-bullet {
      display: none;
      width: 10px;
      height: 5px;
      min-width: 10px;
      min-height: 5px;
      opacity: 1;
      background: rgba(0, 0, 0, 0.2);
      box-sizing: border-box;
    }

    .swiper-pagination-bullet-active {
      background: #5f5cec;
    }
  }
`;

export const TransactionsDescription = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 24px;
  line-height: 36px;
  color: #181938;

  & > p {
    margin-top: 25px;
    color: #5f5cec;
  }

  @media screen and (max-width: 1024px) {
    font-size: 22px;
    line-height: 32px;
  }

  @media screen and (max-width: 670px) {
    font-size: 18px;
    line-height: 25px;
  }
`;
