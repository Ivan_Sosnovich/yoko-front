import http from "shared/api/axios";
import { GetPositionForHistoryReponse, HistoryPositionData } from "./index.types";

export const getHistoryPositions = (count: number, pageNumber: number) =>
  http.get<GetPositionForHistoryReponse>(`position/history/${count}/${pageNumber}`);

export type { GetPositionForHistoryReponse, HistoryPositionData };
