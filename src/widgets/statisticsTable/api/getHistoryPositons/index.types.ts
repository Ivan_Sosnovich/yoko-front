import type { IPosition } from "../../index.types";

export type GetPositionForHistoryReponse = {
  profit: number;
  count: number;
  data: HistoryPositionData[];
};

export type HistoryPositionData = {
  position: Pick<
    IPosition,
    "symbol" | "size" | "closingPrice" | "profit" | "commissions" | "period"
  >;
  positions: Pick<IPosition, "openingTime" | "size" | "openingPrice" | "profit">[];
};
