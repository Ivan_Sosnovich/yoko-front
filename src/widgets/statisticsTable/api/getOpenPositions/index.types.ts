import type { IPosition } from "../../index.types";

export type GetPositionForOpenReponse = {
  nested: number;
  count: number;
  data: OpenPositionData[];
};

export type OpenPositionData = {
  position: Pick<IPosition, "openingTime" | "symbol" | "size" | "targetPrice">;
  positions: Pick<IPosition, "openingTime" | "size" | "openingPrice">[];
};
