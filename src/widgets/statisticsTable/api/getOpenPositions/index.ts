import http from "shared/api/axios";
import type { GetPositionForOpenReponse, OpenPositionData } from "./index.types";

export const getOpenPositions = (count: number, pageNumber: number) =>
  http.get<GetPositionForOpenReponse>(`position/open/${count}/${pageNumber}`);

export type { GetPositionForOpenReponse, OpenPositionData };
