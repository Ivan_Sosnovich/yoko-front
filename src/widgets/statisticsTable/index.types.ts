export type IPosition = {
  openingTime: string;
  symbol: string;
  size: number;
  targetPrice: number;
  openingPrice: number;
  closingPrice: number;
  profit: number;
  commissions: number;
  period: string;
};
