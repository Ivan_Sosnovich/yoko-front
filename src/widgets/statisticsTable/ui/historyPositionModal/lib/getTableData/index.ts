import { ITableProps } from "shared/ui/Table";
import { getFormatDate } from "shared/lib/getFormatDate";
import { HistoryPositionData } from "../../../../api/getHistoryPositons";

export const getTableData = (
  selectPositions: HistoryPositionData["positions"]
): Pick<ITableProps, "headCells" | "rows" | "rowFlexPosition"> => {
  const headCells = [
    {
      width: "25%",
      content: "table.statistics.history.modal.head.date",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.history.modal.head.openingPrice",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.history.modal.head.size",
      position: "center",
      flexPosition: "flex-start",
    },
    {
      width: "20%",
      content: "table.statistics.history.modal.head.profit",
      position: "center",
      flexPosition: "flex-start",
    },
  ];

  const rows: ITableProps["rows"] = selectPositions.map(
    ({ openingPrice, size, openingTime, profit }) => ({
      mobileCard: {
        tableBlock: [
          {
            type: "text",
            title: "table.statistics.history.modal.head.date",
            value: getFormatDate(openingTime),
            position: "left",
            isNotTranslate: true,
          },
          {
            type: "text",
            title: "table.statistics.history.modal.head.openingPrice",
            value: `${openingPrice}`,
            position: "left",
          },
          {
            type: "text",
            title: "table.statistics.history.modal.head.size",
            value: `${size}`,
            position: "left",
          },
          {
            type: "text",
            title: "table.statistics.history.modal.head.profit",
            value: `${profit}`,
            position: "left",
            maxWidth: "110px",
          },
        ],
      },
      cells: [
        {
          width: "25%",
          content: getFormatDate(openingTime),
          position: "center",
          isNotTranslate: true,
          type: "text",
          flexPosition: "flex-start",
        },
        {
          width: "20%",
          content: `${openingPrice}`,
          position: "center",
          isNotTranslate: true,
          type: "text",
          flexPosition: "flex-start",
        },
        {
          width: "20%",
          content: `${size}`,
          position: "center",
          isNotTranslate: true,
          type: "text",
          flexPosition: "flex-start",
        },
        {
          width: "20%",
          content: `${profit}`,
          position: "center",
          isNotTranslate: true,
          type: "text",
          flexPosition: "flex-start",
        },
      ],
    })
  );
  return {
    headCells,
    rows,
    rowFlexPosition: "space-between",
  };
};
