import { useTranslation } from "react-i18next";
import ModalPage from "shared/ui/ModalPage";
import Table from "shared/ui/Table";
import { getTableData } from "../lib/getTableData";
import { HistoryPositionData } from "../../../api/getHistoryPositons";
import * as Styled from "./index.styled";

type IHistoryPositionModalProps = {
  symbol: string;
  selectPositions: HistoryPositionData["positions"];
  onClose: () => void;
};

const HistoryPositionModal = (props: IHistoryPositionModalProps) => {
  const { t } = useTranslation();

  const { selectPositions, onClose, symbol } = props;

  if (!selectPositions?.length) return null;

  const tableData = getTableData(selectPositions);

  return (
    <ModalPage {...{ onClose }}>
      <Styled.HistoryPositionModalWrapper>
        <Styled.HistoryPositionModalInformationBlock>
          <Styled.HistoryPositionModalInformationTitle>
            {t("table.statistics.history.modal.title")}
          </Styled.HistoryPositionModalInformationTitle>
          <Styled.HistoryPositionModalInformationDescription>
            {symbol}
          </Styled.HistoryPositionModalInformationDescription>
        </Styled.HistoryPositionModalInformationBlock>
        <Table {...tableData} />
      </Styled.HistoryPositionModalWrapper>
    </ModalPage>
  );
};

export default HistoryPositionModal;
