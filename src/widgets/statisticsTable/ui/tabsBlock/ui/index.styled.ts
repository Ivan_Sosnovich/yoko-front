import styled from "@emotion/styled";

export const TabBlockWrapper = styled.div`
  width: 100%;
  display: flex;
  align-content: center;
  justify-content: space-between;
`;
