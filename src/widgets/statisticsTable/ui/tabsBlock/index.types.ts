export type ITabs = {
  label: string;
  value: string;
};

export type ITabsBlockProps = {
  handleChangeTab: (newValue: string) => void;
  choiceValue: string;
};
