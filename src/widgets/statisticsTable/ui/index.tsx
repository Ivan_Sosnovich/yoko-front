import { useState } from "react";
import TabsBlock from "./tabsBlock";
import OpenPositions from "./openPositions";
import HistoryPositions from "./historyPositions";
import * as Styled from "./index.styled";

const StatisticsTable = () => {
  const [choiceValue, setChoiceValue] = useState("open");

  const handleChangeTab = (newValue: string) => {
    setChoiceValue(newValue);
  };

  return (
    <Styled.StatisticsTableWrapper>
      <TabsBlock {...{ choiceValue, handleChangeTab }} />
      {choiceValue === "open" && <OpenPositions />}
      {choiceValue === "history" && <HistoryPositions />}
    </Styled.StatisticsTableWrapper>
  );
};

export default StatisticsTable;
