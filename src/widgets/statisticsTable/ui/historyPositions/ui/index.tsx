import { useState } from "react";
import Table from "shared/ui/Table";
import HistoryPositionModal from "../../historyPositionModal";
import { useHistoryPositionsData } from "../../../hooks/useHistoryPositionsData";
import { HistoryPositionData } from "../../../api/getHistoryPositons";

const HistoryPositions = () => {
  const [positions, setPositions] = useState<HistoryPositionData["positions"] | null>(null);
  const [symbol, setSymbol] = useState<string | null>(null);

  const handleClosePositionModal = () => {
    setPositions(null);
  };

  const handleOpenPositionModal = (
    selectPositions: HistoryPositionData["positions"],
    symbol: string
  ) => {
    setPositions(selectPositions);
    setSymbol(symbol);
  };

  const tableData = useHistoryPositionsData({ ...{ handleOpenPositionModal } });

  return (
    <>
      <HistoryPositionModal
        selectPositions={positions}
        onClose={handleClosePositionModal}
        {...{ symbol }}
      />
      <Table {...tableData} />
    </>
  );
};

export default HistoryPositions;
