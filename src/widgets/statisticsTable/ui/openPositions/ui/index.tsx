import { useState } from "react";
import Table from "shared/ui/Table";
import OpenPositionsModal from "../../openPositionModal";
import { useOpenPositionsData } from "../../../hooks/useOpenPositionsData";
import { OpenPositionData } from "../../../api/getOpenPositions";

const OpenPositions = () => {
  const [positions, setPositions] = useState<OpenPositionData["positions"] | null>(null);
  const [symbol, setSymbol] = useState<string | null>(null);

  const handleClosePositionModal = () => {
    setPositions(null);
  };

  const handleOpenPositionModal = (
    selectPositions: OpenPositionData["positions"],
    symbol: string
  ) => {
    setPositions(selectPositions);
    setSymbol(symbol);
  };

  const tableData = useOpenPositionsData({ ...{ handleOpenPositionModal } });

  return (
    <>
      <OpenPositionsModal
        selectPositions={positions}
        onClose={handleClosePositionModal}
        {...{ symbol }}
      />
      <Table {...tableData} />
    </>
  );
};

export default OpenPositions;
