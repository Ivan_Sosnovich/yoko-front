import { useState, useEffect } from "react";
import {
  getOpenPositions,
  GetPositionForOpenReponse,
  OpenPositionData,
} from "../../api/getOpenPositions";
import { getOpenPositionData } from "../../lib/getOpenPositionData";

type IUseOpenPositionsData = {
  handleOpenPositionModal: (selectPositions: OpenPositionData["positions"], symbol: string) => void;
};

export const useOpenPositionsData = (props: IUseOpenPositionsData) => {
  const { handleOpenPositionModal } = props;

  const [openPositionsData, setOpenPositionsData] = useState<GetPositionForOpenReponse | null>(
    null
  );
  const [activePage, setActivePage] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const handleChangeActivePage = (num) => {
    setActivePage(num - 1);
  };

  useEffect(() => {
    setIsLoading(true);
    getOpenPositions(10, activePage)
      .then((data) => setOpenPositionsData(data))
      .catch(() => {
        setOpenPositionsData(null);
      })
      .finally(() => setIsLoading(false));
  }, [activePage]);

  if (!openPositionsData?.data?.length) {
    return {
      emptyMessage: "table.statistics.active.empty.message",
      isLoading,
    };
  }

  return {
    title: "table.statistics.active.title",
    prefix: `${openPositionsData.nested} USDT`,
    handleChangeActivePage,
    activePage,
    isLoading,
    ...getOpenPositionData(openPositionsData, handleOpenPositionModal),
  };
};
