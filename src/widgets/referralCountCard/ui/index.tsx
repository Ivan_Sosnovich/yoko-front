import { useEffect } from "react";
import ShareIcon from "@mui/icons-material/Share";
import { useAppSelector, useAppDispatch } from "providers";
import { getReferralCountAction } from "../model/actions/getReferralCountAction";
import { getReferralCountSelector } from "../model/selectors/getReferralCountSelector";
import { getReferralCountLoadingSelector } from "../model/selectors/getReferralCountLoadingSelector";
import ActionsCard from "shared/ui/actionsCard";

const ReferralCountCard = () => {
  const dispatch = useAppDispatch();

  const referralCount = useAppSelector(getReferralCountSelector);
  const isLoading = useAppSelector(getReferralCountLoadingSelector);

  useEffect(() => {
    dispatch(getReferralCountAction());
  }, []);

  return (
    <ActionsCard
      Icon={ShareIcon}
      title="referral.card.count.title"
      description={`${referralCount ? referralCount : 0}`}
      descriptionColor="#181938"
      {...{ isLoading }}
    />
  );
};

export default ReferralCountCard;
