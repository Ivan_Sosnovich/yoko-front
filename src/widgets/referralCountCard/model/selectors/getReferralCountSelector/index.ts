import { IStore } from "providers";

export const getReferralCountSelector = (state: IStore) => state.referralReducer.referralCount;
