import styled from "@emotion/styled";

export const FooterWrapper = styled.div`
  margin-top: 50px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  gap: 16px;
  width: 100%;
`;

export const FooterText = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.3px;
  color: #9298b8;
`;
