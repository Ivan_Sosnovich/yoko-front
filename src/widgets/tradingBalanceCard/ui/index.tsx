import WalletIcon from "@mui/icons-material/Wallet";
import { useAppSelector } from "providers";
import ActionsCard from "shared/ui/actionsCard";
import { getUserTradingBalanceSelector } from "../model/selectors/getUserTradingBalanceSelector";

const TraidingBalanceCard = () => {
  const tradingBalance = useAppSelector(getUserTradingBalanceSelector);

  return (
    <ActionsCard
      Icon={WalletIcon}
      title="traiding.card.title"
      description={
        Number.isInteger(tradingBalance)
          ? tradingBalance + " USDT"
          : tradingBalance.toFixed(6) + " USDT"
      }
      descriptionColor={tradingBalance > 0 ? "#05CD99" : "#E9002A"}
    />
  );
};

export default TraidingBalanceCard;
