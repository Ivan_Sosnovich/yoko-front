import styled from "@emotion/styled";

export const BotSettingsTableWrapper = styled.div`
  padding: 32px 38px 40px;
  width: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: flex-start;

  @media screen and (max-width: 670px) {
    background: #ffffff;
    padding: 24px 15px;
  }
`;
