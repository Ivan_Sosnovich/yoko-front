import styled from "@emotion/styled";

export const HeaderWrapper = styled.header`
  position: relative;
  z-index: 3;
  background: #ffffff;
  box-shadow: 0 25px 20px -25px rgba(20, 25, 143, 0.1);
  width: 100%;
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 12px 20px;
  box-sizing: border-box;
`;

export const HeaderActionsWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  gap: 16px;
  align-items: center;
  justify-content: flex-end;
`;
