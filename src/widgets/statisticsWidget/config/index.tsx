import { useMemo } from "react";
import { isMobile } from "react-device-detect";
import { IHomePageWrapperProps } from "entities/HomePageWrapper";

export const getStatisticsData = (): IHomePageWrapperProps => {
  // TODO - раскоментировать когда будет аналитика по графику
  // const { t } = useTranslation();

  // const [chartData, setChartData] = useState<number[]>([]);

  // const prefixLine = [
  //   t("chart.january"),
  //   t("chart.february"),
  //   t("chart.march"),
  //   t("chart.april"),
  //   t("chart.may"),
  //   t("chart.june"),
  //   t("chart.july"),
  //   t("chart.august"),
  //   t("chart.september"),
  //   t("chart.october"),
  //   t("chart.november"),
  //   t("chart.december"),
  // ];

  // useEffect(() => {
  //   http.get<number[]>("position/year").then((data) => setChartData(data));
  // });

  return useMemo(() => {
    // const charOptions = {
    //   xAxis: {
    //     type: "category",
    //     data: prefixLine,
    //   },
    //   yAxis: {
    //     type: "value",
    //   },
    //   dataZoom: [
    //     {
    //       type: "inside",
    //       start: isMobile ? 80 : 1,
    //       end: 100,
    //     },
    //   ],
    //   series: [
    //     {
    //       data: chartData,
    //       type: "line",
    //       showBackground: true,
    //       smooth: true,
    //       areaStyle: {
    //         color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    //           {
    //             offset: 0,
    //             color: "rgba(58,77,233,0.8)",
    //           },
    //           {
    //             offset: 1,
    //             color: "rgba(58,77,233,0.3)",
    //           },
    //         ]),
    //       },
    //       itemStyle: {
    //         color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    //           { offset: 0, color: "rgba(95,92,236,1)" },
    //           { offset: 0.5, color: "rgba(95,92,236,0.7987570028011204)" },
    //           { offset: 1, color: "rgba(95,92,236,0.1292892156862745)" },
    //         ]),
    //       },
    //       emphasis: {
    //         itemStyle: {
    //           color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    //             { offset: 0, color: "rgba(95,92,236,1)" },
    //             { offset: 0.7, color: "rgba(95,92,236,0.7987570028011204)" },
    //             { offset: 1, color: "rgba(95,92,236,0.1292892156862745)" },
    //           ]),
    //         },
    //       },
    //     },
    //   ],
    // };
    return {
      title: "text.statistics",
      tableType: "STATISTICS",
      // eChartsOption: charOptions,
    };
  }, [isMobile]);
};
