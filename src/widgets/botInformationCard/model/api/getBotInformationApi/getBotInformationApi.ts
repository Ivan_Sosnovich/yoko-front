import http from "shared/api/axios";
import type { IBotInformation } from "./getBotInformationApi.types";

export const getBotInformationApi = () => http.get<IBotInformation>("bot");
