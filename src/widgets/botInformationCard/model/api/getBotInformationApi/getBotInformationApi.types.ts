import { BotStatus } from "shared/constants/botStatus";

export type IBotInformation = {
  id: number;
  status: BotStatus;
  error?: number;
  isCreated: boolean;
};
