export const getErrorTextForStatusCode = (code: number) => {
  switch (code) {
    case 0:
      return "bot.card.error.0";
    case 1:
      return "bot.card.error.1";
    case 2:
      return "bot.card.error.2";
    default:
      return "";
  }
};
