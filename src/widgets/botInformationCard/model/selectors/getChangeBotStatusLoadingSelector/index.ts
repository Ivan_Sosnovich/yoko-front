import { IStore } from "providers";

export const getChangeBotStatusLoadingSelector = (state: IStore) =>
  state.botReducer.changeBotStatusLoading;
