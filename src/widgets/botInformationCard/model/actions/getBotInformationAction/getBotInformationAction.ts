import { createAsyncThunk } from "@reduxjs/toolkit";
import type { AsyncThunkConfig } from "src/providers/store";
import { getBotInformationApi } from "../../api/getBotInformationApi";
import type { IBotInformation } from "../../api/getBotInformationApi";

export const getBotInformationAction = createAsyncThunk<
  IBotInformation,
  undefined,
  AsyncThunkConfig
>("bot/information", async (_, { rejectWithValue }) => {
  try {
    return await getBotInformationApi();
  } catch (e: any) {
    return rejectWithValue(e);
  }
});
