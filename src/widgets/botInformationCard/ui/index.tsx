import { useEffect, useMemo } from "react";
import { useAppDispatch, useAppSelector } from "providers";
import BotOn from "features/botOn";
import BotOff from "features/botOff";
import { BotStatus } from "shared/constants/botStatus";
import ActionsCard from "shared/ui/actionsCard";
import BotOnIcon from "shared/icons/botIconOn.svg";
import BotOffIcon from "shared/icons/botIconOff.svg";
import BotErrorIcon from "shared/icons/botIconError.svg";
import { getBotStatusSelector } from "../model/selectors/getBotStatusSelector";
import { getChangeBotStatusLoadingSelector } from "../model/selectors/getChangeBotStatusLoadingSelector";
import { getBotErrorSelector } from "../model/selectors/getBotErrorSelector";
import { getUserBalanceSelector } from "../model/selectors/getUserBalanceSelector";
import { getBotInformationAction } from "../model/actions/getBotInformationAction";
import { getErrorTextForStatusCode } from "../model/lib/getErrorTextForStatusCode";

const BotInformationCard = () => {
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector(getChangeBotStatusLoadingSelector);

  const status = useAppSelector(getBotStatusSelector);
  const error = useAppSelector(getBotErrorSelector);
  const balance = useAppSelector(getUserBalanceSelector)!;

  useEffect(() => {
    dispatch(getBotInformationAction());
  }, []);

  const { botIcon, botTitle, info, errorInfo } = useMemo(() => {
    let botIcon = BotOffIcon;
    let botTitle = "bot.card.title.off";
    let info = null;
    let errorInfo = null;

    if (status === BotStatus.CREATE) {
      botIcon = BotOffIcon;
      botTitle = "bot.card.title.create";
      info = "bot.card.information.create";
    }

    if (status === BotStatus.ERROR) {
      botIcon = BotErrorIcon;
      botTitle = "bot.card.title.error";
      errorInfo = getErrorTextForStatusCode(error);
    }

    if (status === BotStatus.OFF) {
      botIcon = BotOffIcon;
      botTitle = "bot.card.title.off";
      if (balance <= 0) {
        info = "bot.card.information.off";
      }
    }

    if (status === BotStatus.ON) {
      botIcon = BotOnIcon;
      botTitle = "bot.card.title.on";
    }

    return {
      botIcon,
      botTitle,
      info,
      errorInfo,
    };
  }, [status]);

  return (
    <ActionsCard
      Icon={botIcon}
      title={botTitle}
      {...{ info, errorInfo, isLoading }}
      actions={[BotOn, BotOff]}
    />
  );
};

export default BotInformationCard;
