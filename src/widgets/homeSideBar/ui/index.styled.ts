import styled from "@emotion/styled";
import { IconButton, Typography } from "@mui/material";

export const HomeMenuButton = styled(IconButton)`
  position: fixed;
  top: 7%;
  left: 1%;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 111;

  @media screen and (max-width: 650px) {
    top: 10%;
  }
`;

interface IHomeSideBarProps {
  isPosition: boolean;
}

export const HomeSideBarWrapper = styled.div<IHomeSideBarProps>`
  padding: 15px 10px;
  width: 276px;
  background: #5f5cec;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  max-height: 100%;

  ${({ isPosition }) =>
    isPosition &&
    `
    position: absolute;
    min-height: auto;
    top: 0;
    left: 0;
    bottom: 0;
    z-index: 1000;
  `}
`;

export const HomeSideBarNavigationBlock = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  gap: 10px;
  box-sizing: border-box;
`;

interface INavigateProps {
  selected: boolean;
}

export const NavigateElement = styled.div<INavigateProps>`
  padding: 5px 5px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 10px;
  box-sizing: border-box;
  width: 228px;
  height: 40px;
  background: ${({ selected }) => (selected ? "rgba(0,0,0,0.1)" : "none")};
  border-radius: ${({ selected }) => (selected ? "10px" : "0")};
  cursor: pointer;
`;

export const NavigateText = styled(Typography)`
  font-weight: 500;
  font-size: 14px;
  line-height: 14px;
  letter-spacing: 0.3px;
  color: #cfcef9;
`;

export const HomeSideBarInfomationBlock = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  gap: 10px;
`;

export const HomeSideBarInfomationTitle = styled(Typography)`
  font-weight: 500;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.3px;
  color: #ffffff;
`;

export const HomeSideBarInfomationDescription = styled(Typography)`
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.3px;
  color: #ffffff;
`;
