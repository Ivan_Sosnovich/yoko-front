import { render } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import App from "./app";
import { store } from "providers";
import "./providers/i18n/config";
import "./index.css";

const container = document.getElementById("root");

render(
  <BrowserRouter>
    <Provider {...{ store }}>
      <App />
    </Provider>
  </BrowserRouter>,
  container
);
