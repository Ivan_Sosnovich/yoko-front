import { lazy, Suspense } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import AppLayout from "app/layouts/HomeLayout";
import MainLayout from "app/layouts/MainLayout";

import Loading from "shared/ui/Loading";
import { PATH } from "shared/constants/routes";
import { useAuth } from "app/hooks";

const MainPage = lazy(() => import("pages/main"));
const HomePage = lazy(() => import("pages/home"));
const ReferralPage = lazy(() => import("pages/referral"));
const PositionPage = lazy(() => import("pages/position"));
const PaymentPage = lazy(() => import("pages/payment"));

export const AppRoute = () => {
  const isAuth = useAuth();

  if (isAuth) {
    return (
      <AppLayout>
        <Suspense fallback={<Loading />}>
          <Routes>
            <Route path={PATH.HOME} element={<HomePage />} />
            <Route path={PATH.REFERRAL} element={<ReferralPage />} />
            <Route path={PATH.POSITION} element={<PositionPage />} />
            <Route path={PATH.PAYMENT} element={<PaymentPage />} />
            <Route path="*" element={<Navigate to={PATH.HOME} key="*" />} />
          </Routes>
        </Suspense>
      </AppLayout>
    );
  }
  return (
    <Suspense fallback={<Loading />}>
      <MainLayout>
        <Routes>
          <Route path={PATH.MAIN} element={<MainPage />} />
          <Route path="*" element={<Navigate to={PATH.MAIN} key="*" />} />
        </Routes>
      </MainLayout>
    </Suspense>
  );
};
