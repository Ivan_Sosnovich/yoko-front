import { createSlice } from "@reduxjs/toolkit";
import { getReferralCountAction } from "widgets/referralCountCard";
import { InitialState } from "./index.types";

const initialState: InitialState = {
  loading: false,
  error: null,
  referralCount: null,
};

const botSlice = createSlice({
  name: "referral",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getReferralCountAction.fulfilled, (state, { payload }) => {
      state.referralCount = payload.count;
      state.loading = false;
    });
    builder.addCase(getReferralCountAction.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getReferralCountAction.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default botSlice.reducer;
