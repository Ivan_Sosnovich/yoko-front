export interface InitialState {
  loading: boolean;
  error?: string | null;
  referralCount: number | null;
}
