import type { IBotInformation } from "widgets/botInformationCard";
import { IChangeBotKeysData } from "features/changeBotKeys";
import { IBotSettingsData } from "features/changeBotSettings";

export interface InitialState {
  loading: boolean;
  error?: string | null;
  botInformation: IBotInformation | null;
  keys: IChangeBotKeysData | null;
  changeBotKeysLoading: boolean;
  settings: IBotSettingsData | null;
  changeBotSettingsLoading: boolean;
  changeBotStatusLoading: boolean;
}
