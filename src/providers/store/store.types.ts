import { store } from "./store";
import { InitialState as UserState } from "./user";
import { InitialState as BotState } from "./bot";
import { InitialState as ReferralState } from "./referral";

export interface IStore {
  userReduser: UserState;
  botReducer: BotState;
  referralReducer: ReferralState;
}

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export interface AsyncThunkConfig {
  dispatch: AppDispatch;
  state: IStore;
  rejectValue: string | null;
}
