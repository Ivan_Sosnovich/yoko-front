import { createSlice } from "@reduxjs/toolkit";
import { sessionUser } from "app/hooks";
import { cleanUseAuthErrorAction } from "features/userAuth";
import { userLogOutAction } from "features/userLogout";
import { transferReferralBalanceAction } from "features/transferReferralBalance";
import { loginUser } from "features/userAuth";
import { LOCAL_STORAGE_TOKEN_NAME } from "shared/constants/variable";
import type { InitialState } from "./index.types";

const initialState: InitialState = {
  user: null,
  loading: false,
  error: null,
  confirmationLoading: false,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(userLogOutAction, (state) => {
      window.localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
      state.user = null;
    });
    builder.addCase(cleanUseAuthErrorAction, (state) => {
      state.error = null;
    });
    builder.addCase(loginUser.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(loginUser.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
    builder.addCase(loginUser.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.user = payload;
    });
    builder.addCase(sessionUser.pending, (state) => {
      if (!state.user) {
        state.loading = true;
      }
    });
    builder.addCase(sessionUser.fulfilled, (state, { payload }) => {
      if (!state.user) {
        state.user = payload;
      }
      state.loading = false;
    });
    builder.addCase(sessionUser.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(transferReferralBalanceAction.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(transferReferralBalanceAction.fulfilled, (state, { payload }) => {
      state.user.balance = +state.user.balance + +payload.deposit;
      state.user.referralBalance = state.user.referralBalance - payload.deposit;
      state.loading = false;
    });
    builder.addCase(transferReferralBalanceAction.rejected, (state, { payload }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default userSlice.reducer;
