// перевод
export { default } from "./i18n/config";
// тема
import CustomTheme from "./theme";
export { CustomTheme };
// стор
export { store, useAppDispatch, useAppSelector } from "./store";
export type { IStore, RootState, AppDispatch, AsyncThunkConfig } from "./store/store.types";
// роутинг
export { AppRoute } from "./routers";
