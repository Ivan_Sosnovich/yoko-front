import MainHeader from "widgets/mainHeader";
import MainFooter from "widgets/mainFooter";
import BonusCard from "./BonusCard";

const LendingLayout = (props) => {
  const { children } = props;

  return (
    <>
      <BonusCard />
      <MainHeader />
      {children}
      <MainFooter />
    </>
  );
};

export default LendingLayout;
