import styled from "@emotion/styled";

export const LendingLayoutWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  min-height: 100vh;
  height: 100vh;
  max-width: 1920px;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: flex-start;
`;
