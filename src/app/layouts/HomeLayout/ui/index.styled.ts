import styled from "@emotion/styled";

export const AppLayoutWrapper = styled.div`
  overflow: hidden;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
`;

export const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-content: flex-start;
  justify-content: flex-start;
  overflow: hidden;
  background: #f4f7fe;
  position: relative;
`;

export const ChildWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  width: 100%;
  min-height: 100%;
  overflow: hidden;
  overflow-y: auto;
  padding: 13px 40px 40px 40px;
  box-sizing: border-box;

  @media screen and (max-width: 1024px) {
    padding: 13px 20px 20px 20px;
  }
`;

export const PageWrapper = styled.div`
  flex: 5 1 100%;
`;

export const PageFooter = styled.div`
  flex: 2 1 5%;
  height: 5%;
`;
