import { useNavigate } from "react-router-dom";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getUserApi, IUser } from "shared/api/user/getUser";
import { sessionApi } from "shared/api/user/session";
import type { AsyncThunkConfig } from "providers";

export const sessionUser = createAsyncThunk<IUser, undefined, AsyncThunkConfig>(
  "user/session",
  async (_, { rejectWithValue }) => {
    try {
      await sessionApi();
      return await getUserApi();
    } catch (e: any) {
      const navigate = useNavigate();
      navigate("");
      rejectWithValue(e);
    }
  }
);
