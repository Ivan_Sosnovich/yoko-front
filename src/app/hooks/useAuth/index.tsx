import { useMemo } from "react";
import { useAppSelector } from "providers";
import { getUserSelector } from "./model/selectors/getUserSelector";

const useAuth = () => {
  const userId = useAppSelector(getUserSelector);

  return useMemo(() => !!userId, [userId]);
};

export default useAuth;
