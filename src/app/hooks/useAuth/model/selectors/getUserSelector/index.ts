import { IStore } from "providers";

export const getUserSelector = (state: IStore) => state.userReduser.user?.id;
