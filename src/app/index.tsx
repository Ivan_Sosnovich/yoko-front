import Favicon from "react-favicon";
import ThemeProvider from "@mui/material/styles/ThemeProvider";
import { CssBaseline } from "@mui/material";
import { CustomTheme, AppRoute } from "providers";
import ConfirmationUser from "features/confirmationUser";
import RecoveryPasswordWrapper from "features/recoveryPassword";
import IconPath from "shared/icons/logo.png";
import ErrorBoundary from "./ui/ErrorBoundary";
import { useSession } from "./hooks";

const App = () => {
  useSession();

  return (
    <ErrorBoundary>
      <>
        <ThemeProvider theme={CustomTheme}>
          <ConfirmationUser />
          <RecoveryPasswordWrapper />
          <CssBaseline />
          <AppRoute />
        </ThemeProvider>
        <Favicon url={IconPath} />
      </>
    </ErrorBoundary>
  );
};

export default App;
